/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;
import static net.java.quickcheck.srcgenerator.Language.*;

class SampleClassRenderer extends BaseClassRenderer{

	public SampleClassRenderer(ClassInfo info) {
		super(info);
	}
	
	@Override
	public String getGeneratedClassName() {
		return singular(info.className) + "Samples";
	}
	
	@Override
	protected String methodName(Method method) {
		return "any" + singular(method.name);
	}
	
	@Override
	protected String methodContent(Method method) {
		return format("return %s.next();", generatorInstance(method));
	}

	@Override
	protected String returnType(Method method) {
		return method.returnType.name;
	}
}