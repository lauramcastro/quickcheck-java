/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;

final class Type {

	final String name;
	final boolean generic;

	public Type(CharSequence name, boolean generic) {
		Assertion.assertNotNull(name, "name");
		this.name = name.toString();
		this.generic = generic;
	}
	
	@Override
	public String toString() {
		return format("Type[name=%s, generic=%s", name, generic);
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Type)){
			return false;
		}
		Type other = (Type) obj;
		return other.name.equals(name) && other.generic == generic;
	}
}
