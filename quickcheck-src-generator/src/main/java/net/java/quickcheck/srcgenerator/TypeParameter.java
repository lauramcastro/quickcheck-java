/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;
import java.util.List;

import static net.java.quickcheck.srcgenerator.Assertion.*;

import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.TypeMirror;

final class TypeParameter {

	final String type;
	final String bound;

	TypeParameter(String type, String bound) {
		this.type = type;
		this.bound = bound;
	}

	TypeParameter(TypeParameterElement typeParameter) {
		Assertion.assertNotNull(typeParameter, "typeParameter");
		this.type = getTypeString(typeParameter);
		this.bound = getBound(typeParameter);
		Assertion.assertNotNull(type, "type");
	}

	private static String getBound(TypeParameterElement typeParameter) {
        List<? extends TypeMirror> bounds = typeParameter.getBounds();
        assertTrue(typeParameter.getBounds().size() < 2, "Only 1 bound argument supported.");
        return bounds.isEmpty() ? null : bounds.get(0).toString();
	}

	private static String getTypeString(TypeParameterElement typeParameter) {
		return typeParameter.toString();
	}

	@Override
	public int hashCode() {
		return type == null ? 0 : type.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof TypeParameter)) {
			return false;
		}
		TypeParameter other = (TypeParameter) obj;

		if(bound == null) {
			if(other.bound != null) return false;
		} else if(!bound.equals(other.bound)) return false;
		return type.equals(other.type);
	}
	
	@Override
	public String toString() {
		return format("TypeParameter[type=%s, bound=%s]", type == null ? "" : type, bound == null ? "" : bound);
	}
}
