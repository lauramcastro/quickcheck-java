/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;

import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;

import net.java.quickcheck.srcgenerator.Traversal.DecliningTypeVisitor;

final class Parameter {
	String name;
	String type;
	Cardinality cardinality;
	
	enum Cardinality{ARRAY, VARARRAY, ONE};

	Parameter(CharSequence name, CharSequence type, Cardinality cardinality) {
		Assertion.assertNotNull(name, "name");
		Assertion.assertNotNull(type, "type");
		Assertion.assertNotNull(cardinality, "cardinality");
		this.name = name.toString();
		this.type = type.toString();
		this.cardinality = cardinality;
	}
	
	Parameter(CharSequence name, CharSequence type){
		this(name, type, Cardinality.ONE);
	}
	
	public Parameter(VariableElement v, Cardinality cardinality) {
		TypeMirror parameterType = v.asType();
		boolean anArray = parameterType.accept(new DecliningTypeVisitor<Boolean>(){
			@Override
			public Boolean visitDeclared(DeclaredType t, Object p) {
				return false;
			}
			@Override
			public Boolean visitPrimitive(PrimitiveType t, Object p) {
				return false;
			}
			
			@Override
			public Boolean visitArray(ArrayType t, Object p) {
				return true;
			}
			
			@Override
			public Boolean visitTypeVariable(TypeVariable t, Object p) {
				return false;
			}
		}, null);
		this.cardinality = anArray ? cardinality : Cardinality.ONE;
		this.type = anArray ? ((ArrayType) parameterType).getComponentType().toString() : parameterType.toString();
		this.name = v.getSimpleName().toString();
	}

	@Override public boolean equals(Object obj){
		if(!(obj instanceof Parameter)){
			return false;
		}
		Parameter other = (Parameter) obj;
		return name.equals(other.name) && type.equals(other.type) && cardinality == other.cardinality;
	}
	
	@Override public String toString() { 
		return format("Parameter[name=%s, type=%s, cardinality=%s]", name, type, cardinality);
	}
	
	@Override public int hashCode() {
		return name.hashCode(); 
	}
}
