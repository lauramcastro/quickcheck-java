/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;
import static net.java.quickcheck.srcgenerator.Parameter.Cardinality.*;
import net.java.quickcheck.srcgenerator.Parameter.Cardinality;

abstract class BaseClassRenderer implements ClassRenderer{

	ClassInfo info;
	private int level = 0;

	BaseClassRenderer(ClassInfo info) {
		this.info = info;
	}

	@Override
	public String build() {
		StringBuilder b = new StringBuilder();

		writeln(b, "package %s;\n", info.packageName);
		write(b, "public class %s", getGeneratedClassName());
		openBrace(b);
		for(Method m : info.methods) writeMethod(b, m);
		closeBrace(b);

		return b.toString();
	}

	void writeMethod(StringBuilder b, Method method) {
		writeMethodDocumentation(b, method);
		writeMethodSignature(b, method);
		openBrace(b);
		writeln(b, methodContent(method));
		closeBrace(b);
	}

	private void writeMethodDocumentation(StringBuilder b, Method method) {
		writeln(b, "");
		writeln(b, "/**");
		writeln(b, "See documentation of {@link %s.%s#%s}.", info.packageName, info.className, method.name, parameterTypes(method));
		writeln(b, "*/");
	}

	protected abstract String methodContent(Method method);

	private void writeMethodSignature(StringBuilder b, Method method) {
		write(b, "public static %s %s %s(%s)", typeParameters(method), returnType(method), methodName(method),
				parameters(method));
	}

	protected abstract String methodName(Method method);

	String typeParameter(Type returnType) {
		StringBuilder r = new StringBuilder();
		if(returnType.generic) r.append("<").append(returnType.name).append("> ");
		return r.toString();
	}

	protected abstract String returnType(Method method);

	String typeParameters(Method method) {
		if(method.typeParameters.isEmpty()) return "";
		StringBuilder r = new StringBuilder();
		r.append("<");
		for(int i=0;i<method.typeParameters.size();i++) {
			r.append(typeParameter(method.typeParameters.get(i)));
			if(i != method.typeParameters.size() - 1) r.append(",");
		}
		r.append("> ");
		return r.toString();
	}

	private String typeParameter(TypeParameter typeParameter) {
		String bound = typeParameter.bound;
		return bound == null ? typeParameter.type : format("%s extends %s", typeParameter.type, bound);
	}

	String arguments(Method method) {
		StringBuilder ps = new StringBuilder();
		for(Parameter p : method.parameters) {
			if(ps.length() != 0) ps.append(", ");
			ps.append(p.name);
		}
		return ps.toString();
	}

	String parameterTypes(Method method) {
		StringBuilder ps = new StringBuilder();
		for(Parameter p : method.parameters) {
			if(ps.length() != 0) ps.append(", ");
			ps.append(p.type);
		}
		return ps.toString();
	}
	
	String parameters(Method method) {
		StringBuilder ps = new StringBuilder();
		for(Parameter p : method.parameters) {
			if(ps.length() != 0) ps.append(", ");
			ps.append(format("%s%s %s", p.type, parameterCardinality(p.cardinality), p.name));
		}
		return ps.toString();
	}

	private String parameterCardinality(Cardinality cardinality) {
		if(cardinality == ARRAY) return "[]";
		if(cardinality == VARARRAY) return "...";
		return "";
	}

	void writeln(StringBuilder w, String template, Object... args) {
		write(w, template, args);
		write(w, "\n");
	}

	void write(StringBuilder w, String template, Object... args) {
		for(int i = 0; i < level; i++) w.append("    ");
		w.append(format(template, args));
	}

	void openBrace(StringBuilder w) {
		w.append("{\n");
		level++;
	}

	void closeBrace(StringBuilder w) {
		level--;
		writeln(w, "}");
	}

	String generatorInstance(Method method) {
		return format("%s.%s.%s%s(%s)", info.packageName, info.className, typeParameter(method.returnType),
				method.name, arguments(method));
	}
}
