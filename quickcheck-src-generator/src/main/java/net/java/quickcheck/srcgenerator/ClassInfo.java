/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;
import static java.util.Arrays.*;
import static javax.lang.model.element.Modifier.*;
import static javax.lang.model.util.ElementFilter.*;
import static net.java.quickcheck.srcgenerator.Assertion.*;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;

class ClassInfo {
	
	private static final Set<Modifier> METHOD_MODIFIERS = EnumSet.of(STATIC, PUBLIC);
	final CharSequence className;
	final CharSequence packageName;
	final Set<Method> methods = new HashSet<Method>();

	ClassInfo(ProcessingEnvironment env, Element annotatedElement) {
		TypeElement classDeclaration = classDeclaration(annotatedElement);
		this.packageName = packageName(env, classDeclaration);
		this.className = classDeclaration.getSimpleName();
		
		for(ExecutableElement method : findMethods(classDeclaration)) {
			methods.add(new Method(env.getMessager(), method));
		}
	}

	ClassInfo(Name packageName, Name className, Method... methods) {
		this.packageName = packageName;
		this.className = className;
		this.methods.addAll(asList(methods));
	}

	private Name packageName(ProcessingEnvironment env, TypeElement classDeclaration) {
		return env.getElementUtils().getPackageOf(classDeclaration).getQualifiedName();
	}
	
	private List<ExecutableElement> findMethods(Element classDeclaration) {
		List<ExecutableElement> es = new ArrayList<ExecutableElement>();
		for(ExecutableElement executable : methodsIn(classDeclaration.getEnclosedElements())) {
			if(executable.getModifiers().containsAll(METHOD_MODIFIERS)) es.add(executable);
		}
		return es;
	}

	private TypeElement classDeclaration(Element annotatedElements) {
		TypeElement type = Traversal.toTypeElement(annotatedElements);
		assertTrue(type.getKind() == ElementKind.CLASS,"Its only allowed to annotate classes.");
		return type;
	}

	@Override
	public boolean equals(Object obj) {
		ClassInfo info = (ClassInfo) obj;
		return info.className.equals(className) && info.packageName.equals(packageName) && info.methods.equals(methods);
	}

	@Override
	public int hashCode() {
		return className.hashCode();
	}
	
	@Override
	public String toString() {
		return format("ClassInfo[className=%s, packageName=%s, methods=%s]", className, packageName, methods);
	}
}
