/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ErrorType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.NullType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.UnionType;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.AbstractTypeVisitor7;

class Traversal {

	static DeclaredType toDeclaredType(TypeMirror type) {
		return type.accept(new DecliningTypeVisitor<DeclaredType>(){
			@Override public DeclaredType visitDeclared(DeclaredType t, Object arg) {
				return t;
			}
		}, null);
	}

	static TypeElement toTypeElement(TypeMirror type) {
		return toTypeElement(toDeclaredType(type).asElement());
	}

	static TypeElement toTypeElement(Element element) {
		return element.accept(new DecliningElementVisitor<TypeElement>(){
			@Override
			public TypeElement visitType(TypeElement e, Object p) {
				return e;
			}
		}, null);
	}
	
	static class DecliningElementVisitor<T> implements ElementVisitor<T, Object> {

		@Override public T visit(Element e) { throw new UnsupportedOperationException(); }
		@Override public T visit(Element e, Object p) { throw new UnsupportedOperationException(); }
		@Override public T visitExecutable(ExecutableElement e, Object p) { throw new UnsupportedOperationException(); }
		@Override public T visitPackage(PackageElement e, Object p) { throw new UnsupportedOperationException(); }
		@Override public T visitType(TypeElement e, Object p) { throw new UnsupportedOperationException(); }
		@Override public T visitTypeParameter(TypeParameterElement e, Object p) { throw new UnsupportedOperationException(); }
		@Override public T visitUnknown(Element e, Object p) { throw new UnsupportedOperationException(); }
		@Override public T visitVariable(VariableElement e, Object p) { throw new UnsupportedOperationException(); }
	}
	
	static class DecliningTypeVisitor<R> extends AbstractTypeVisitor7<R, Object> {
		
		@Override public R visitArray(ArrayType t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitDeclared(DeclaredType t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitError(ErrorType t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitExecutable(ExecutableType t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitNoType(NoType t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitNull(NullType t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitPrimitive(PrimitiveType t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitTypeVariable(TypeVariable t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitUnknown(TypeMirror t, Object p) { throw new UnsupportedOperationException(); }
		@Override public R visitWildcard(WildcardType t, Object p) { throw new UnsupportedOperationException(); }
	        @Override public R visitUnion(UnionType t, Object p) { throw new UnsupportedOperationException(); }
	}

}
