/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;
import static java.util.Collections.*;
import static javax.tools.Diagnostic.Kind.*;

import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Set;

import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;

public abstract class BaseProcessor implements Processor{

	protected abstract Class<? extends Annotation> getSupportedAnnotationType();

	protected abstract ClassRenderer createRenderer(ClassInfo info);
	
	private ProcessingEnvironment env;

	@Override
	public boolean process(Set<? extends TypeElement> annotatedType, RoundEnvironment roundEnv) {
		Set<? extends Element> annotated = roundEnv.getElementsAnnotatedWith(getSupportedAnnotationType());
		if(roundEnv.processingOver() || annotated.isEmpty()) return false;
		for(Element e : annotated) processType(e); 
		return false;
	}

	private void processType(Element e) {
		ClassInfo info = new ClassInfo(env, e);
		ClassRenderer builder = createRenderer(info);
		String generatedType = builder.getGeneratedClassName();
		writeToFile(format("%s.%s", info.packageName, generatedType), builder.build());
		env.getMessager().printMessage(NOTE, "wrote " + generatedType);
	}
	
	private void writeToFile(String generatedType, String code) {
		try {
			Writer out = env.getFiler().createSourceFile(generatedType).openWriter();
			out.write(code);
			out.close();
			env.getMessager().printMessage(NOTE, "wrote " + generatedType);
		} catch(IOException e) {
			env.getMessager().printMessage(ERROR, e.getMessage());
		}
	}

	@Override
	public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation,
			ExecutableElement member, String userText) {
		return Collections.<Completion> emptyList();
	}

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		return singleton(getSupportedAnnotationType().getName());
	}


	@Override
	public Set<String> getSupportedOptions() {
		return Collections.emptySet();
	}

	@Override
	public SourceVersion getSupportedSourceVersion() {
	        return SourceVersion.latestSupported();
	}

	@Override
	public void init(ProcessingEnvironment processingEnv) {
		env = processingEnv;
	}

}
