/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 package net.java.quickcheck.srcgenerator;

import static java.lang.Character.*;

abstract class Language {
	
	/**
	 * Simple singular algorithm. Returns the last character.
	 */
	static CharSequence singular(CharSequence name) {
		if(name.length() < 2) return "";
		boolean regularPlural = name.charAt(name.length() - 1) == 's';
		return capitalize(regularPlural ? name.subSequence(0, name.length() - 1) : name);
	}
	
	/**
	 * {@see CG}
	 */
	static CharSequence capitalize(CharSequence name) {
		return Character.toString(toUpperCase(name.charAt(0))) + name.subSequence(1, name.length());
	}
}
