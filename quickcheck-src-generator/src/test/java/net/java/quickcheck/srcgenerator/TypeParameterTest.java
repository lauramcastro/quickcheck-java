/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static junit.framework.Assert.*;
import static net.java.quickcheck.srcgenerator.Model.*;

import java.util.Arrays;
import java.util.Collections;

import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;

import org.junit.Test;

public class TypeParameterTest {

	@Test
	public void typeParameter(){
		TypeParameterElement typeParameterElement = typeParameterElement();
		TypeParameter typeParameter = new TypeParameter(typeParameterElement);
		assertEquals(typeParameter, new TypeParameter(typeParameterElement.toString(), null));
	}

	@Test
	public void typeParameterBounds(){
		DeclaredType bound = anyDeclaredType();
		TypeParameterElement typeParameterElement = typeParameterElement(Collections.<TypeMirror> singletonList(bound));
		TypeParameter typeParameter = new TypeParameter(typeParameterElement);
		assertEquals(typeParameter, new TypeParameter(typeParameterElement.toString(), bound.toString()));
	}
	
	@Test(expected=InvalidAnnotationException.class)
	public void typeParameterBoundsHasOneElement(){
		TypeParameterElement typeParameterElement = typeParameterElement(Arrays.<TypeMirror> asList(anyDeclaredType(),
				anyDeclaredType()));
		new TypeParameter(typeParameterElement);
	}

	private DeclaredType anyDeclaredType() {
		return declaredType(null);
	}
}
