/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.util.EnumSet.*;
import static javax.lang.model.element.ElementKind.*;
import static javax.lang.model.element.Modifier.*;
import static net.java.quickcheck.srcgenerator.Method.*;
import static net.java.quickcheck.srcgenerator.Model.*;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Set;

import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.util.Elements;

import org.easymock.EasyMock;
import org.junit.Test;

public class ClassInfoTest {

	private ProcessingEnvironment env = createMock(ProcessingEnvironment.class);
	
	@Test(expected=InvalidAnnotationException.class)
	public void anInterfaceIsInvalid(){
		new ClassInfo(env, interfaceElement("className"));
	}

	@Test
	public void annotatedTypeWithoutMethods(){
		TypeElement typeElement = classElement("SomeGenerators");
		PackageElement packageElement = packageElement();
		
		expectGetPackageOf(typeElement, packageElement);
		
		ClassInfo actual = new ClassInfo(env, typeElement);
		
		ClassInfo expected = new ClassInfo(packageElement.getQualifiedName(), typeElement
				.getSimpleName());
		assertEquals(expected, actual);
	}
	
	@Test
	public void string(){
		ClassInfo expected = new ClassInfo(name("x"), name("y"));
		assertNotNull(expected.toString());
	}

	@Test
	public void annotatedTypeWithMethod() {

		DeclaredType returnTypeTypeArguments = declaredType(classElement("returnTypeName"));
		DeclaredType returnType = declaredType(interfaceElement(GENERATOR_TYPE_NAME), returnTypeTypeArguments);

		ExecutableElement methodExecutableElement = methodExecutableElement(
				"methodSimpleName", returnType);

		TypeElement typeElement = classElement("className", methodExecutableElement);
		PackageElement packageElement = packageElement();

		expectGetPackageOf(typeElement, packageElement);
		ClassInfo info = new ClassInfo(env, typeElement);

		Set<Method> expected = Collections.singleton(new Method(methodExecutableElement
				.getSimpleName(), returnTypeTypeArguments.toString()));
		Set<Method> actual = info.methods;
		assertEquals(expected, actual);
	}
	
	@Test
	public void annotatedTypeWithNonStaticMethod() {
		ExecutableElement methodExecutableElement = methodExecutableElement(name("methodSimpleName"), METHOD,
				declaredType(classElement("returnTypeName")), of(PUBLIC) , new TypeParameterElement[0],
				new VariableElement[] {});

		TypeElement typeElement = classElement("className", methodExecutableElement);
		expectGetPackageOf(typeElement, packageElement());
		ClassInfo info = new ClassInfo(env, typeElement);

		assertEquals(Collections.<Method> emptySet(), info.methods);
	}
	
	@Test
	public void annotatedTypeWithNonPrivateStaticMethod() {
		ExecutableElement methodExecutableElement = methodExecutableElement(name("methodSimpleName"), METHOD,
				declaredType(classElement("returnTypeName")), of(PRIVATE, STATIC) , new TypeParameterElement[0],
				new VariableElement[] {});

		TypeElement typeElement = classElement("className", methodExecutableElement);
		expectGetPackageOf(typeElement, packageElement());
		ClassInfo info = new ClassInfo(env, typeElement);

		assertEquals(Collections.<Method> emptySet(), info.methods);
	}
	
	private PackageElement packageElement() {
		PackageElement packageElement = createMock(PackageElement.class);
		expect(packageElement.getQualifiedName()).andReturn(name("")).anyTimes();
		return replay(packageElement);
	}

	private <T> T replay(T name) {
		EasyMock.replay(name);
		return name;
	}
 
	private void expectGetPackageOf(TypeElement typeElement,
			PackageElement packageElement) {
		Elements elements = createMock(Elements.class);
		expect(elements.getPackageOf(typeElement)).andReturn(packageElement);
		expect(env.getElementUtils()).andReturn(elements);
		Messager messager = createNiceMock(Messager.class);
		expect(env.getMessager()).andStubReturn(messager);
		EasyMock.replay(env,elements, messager);
	}
}
