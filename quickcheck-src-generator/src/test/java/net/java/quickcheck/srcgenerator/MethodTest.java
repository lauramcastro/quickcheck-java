/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static java.util.Collections.*;
import static net.java.quickcheck.srcgenerator.Method.*;
import static net.java.quickcheck.srcgenerator.Model.*;
import static net.java.quickcheck.srcgenerator.Parameter.Cardinality.*;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.util.Collections;

import javax.annotation.processing.Messager;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeVariable;

import org.junit.Test;

public class MethodTest {

	@Test
	public void valid() {
		DeclaredType typeArgument = declaredType(classElement("returnTypeName"));
		DeclaredType validReturnType = generatorInterface(typeArgument);
		String name = "methodSimpleName";
		ExecutableElement methodExecutableElement = Model.methodExecutableElement(name, validReturnType);
		Method actual = method(methodExecutableElement);
		Method expected = new Method(name, typeArgument.toString());
		assertEquals(expected, actual);
	}
	
	@Test
	public void string(){
		Method expected = new Method("x","y");
		assertNotNull(expected.toString());
	}


	@Test
	public void methodWithVarArrayParameters() {
		DeclaredType typeArgument = declaredType(classElement("returnTypeName"));
		DeclaredType validReturnType = generatorInterface(typeArgument);
		DeclaredType parameterType = declaredType(interfaceElement("parameterType"));
		Parameter parameter  = new Parameter("parameter", parameterType.toString());
		TypeVariable componentType = typeVariable();
		Parameter anotherParameter  = new Parameter("anotherParameter", componentType.toString(), VARARRAY);
		ExecutableElement methodExecutableElement = Model.methodExecutableElement("methodSimpleName", validReturnType,
				variableElement(parameter.name, parameterType), 
				variableElement(anotherParameter.name, arrayType(componentType)));
		Method actual = method(methodExecutableElement);
		Method expected = new Method("methodSimpleName", typeArgument.toString(), parameter, anotherParameter );
		assertEquals(expected, actual);
	}

	@Test
	public void methodWithArrayParameterThatIsNotInVarArgPosition() {
		DeclaredType typeArgument = declaredType(classElement("returnTypeName"));
		DeclaredType validReturnType = generatorInterface(typeArgument);
		DeclaredType parameterType = declaredType(interfaceElement("parameterType"));
		Parameter parameter = new Parameter("parameter", parameterType.toString());
		TypeVariable componentType = typeVariable();
		ArrayType anotherParameterType = arrayType(componentType);
		Parameter anotherParameter = new Parameter("anotherParameter", componentType.toString(), ARRAY);
		ExecutableElement methodExecutableElement = Model.methodExecutableElement("methodSimpleName", validReturnType,
				variableElement(anotherParameter.name, anotherParameterType), variableElement(parameter.name,
						parameterType));
		Method actual = method(methodExecutableElement);
		Method expected = new Method("methodSimpleName", typeArgument.toString(),anotherParameter, parameter);
		assertEquals(expected, actual);
	}
	
	@Test
	public void methodWithTypeParameters() {
		DeclaredType typeArgument = declaredType(classElement("returnTypeName"));
		DeclaredType validReturnType = generatorInterface(typeArgument);
		String name = "methodSimpleName";
		TypeParameterElement typeParameter = typeParameterElement();
		ExecutableElement methodExecutableElement = Model.methodExecutableElement(name, validReturnType, typeParameter);
		Method actual = method(methodExecutableElement);
		Method expected = new Method(name, typeArgument.toString(), singletonList(new TypeParameter(typeParameter)),
				Collections.<Parameter> emptyList(), false);
		assertEquals(expected, actual);
	}

	@Test(expected = InvalidAnnotationException.class)
	public void returnTypeHasToBeAGenerator() {
		DeclaredType invalidReturnType = declaredType(interfaceElement("invalid return type"));
		ExecutableElement methodExecutableElement = methodExecutableElement(invalidReturnType);
		method(methodExecutableElement);
	}

	private ExecutableElement methodExecutableElement(DeclaredType returnType) {
		return Model.methodExecutableElement("methodSimpleName", returnType);
	}

	@Test(expected = InvalidAnnotationException.class)
	public void returnTypeHasToBeAInterface() {
		DeclaredType invalidReturnType = declaredType(interfaceElement("invalid return type"));
		ExecutableElement executableElement = methodExecutableElement(invalidReturnType);
		method(executableElement);
	}

	@Test(expected = InvalidAnnotationException.class)
	public void returnTypeOnlyInterfaceSupported() {
		DeclaredType invalidReturnType = declaredType(classElement(Method.GENERATOR_TYPE_NAME),
				declaredType(classElement("returnTypeName")));
		ExecutableElement executableElement = methodExecutableElement(invalidReturnType);
		method(executableElement);
	}

	@Test
	public void returnTypeIsGeneric() {
		TypeVariable typeVariable = typeVariable();
		DeclaredType returnType = declaredType(interfaceElement(Method.GENERATOR_TYPE_NAME), typeVariable);
		ExecutableElement methodExecutableElement = methodExecutableElement(returnType);
		Method actual = method(methodExecutableElement);
		Method expected = new Method(methodExecutableElement.getSimpleName(), typeVariable.toString(),
				Collections.<TypeParameter> emptyList(), Collections.<Parameter> emptyList(), true);
		assertEquals(actual, expected);
	}

	@Test
	public void returnTypeIsSubtypeOfGenerator() {
		DeclaredType generatorInterface = declaredType(interfaceElement(GENERATOR_TYPE_NAME));
		TypeElement genertorSubType = interfaceElement("some sub type", generatorInterface);

		DeclaredType returnTypeTypeArgument = declaredType(classElement("returnTypeName"));
		DeclaredType returnType = declaredType(genertorSubType, returnTypeTypeArgument);

		ExecutableElement methodExecutableElement = methodExecutableElement(returnType);

		Method actual = method(methodExecutableElement);
		Method expected = new Method(methodExecutableElement.getSimpleName(), returnTypeTypeArgument.toString());

		assertEquals(expected, actual);
	}

	private Method method(ExecutableElement methodExecutableElement) {
		return new Method(environment(), methodExecutableElement);
	}

	private Messager environment() {
		Messager messager = createNiceMock(Messager.class);
		replay(messager);
		return messager;
	}
	
	private DeclaredType generatorInterface(DeclaredType typeArguments) {
		return declaredType(interfaceElement(GENERATOR_TYPE_NAME), typeArguments);
	}
}
