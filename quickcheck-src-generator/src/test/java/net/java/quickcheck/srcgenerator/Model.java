/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 package net.java.quickcheck.srcgenerator;

import static java.lang.String.*;
import static java.util.Arrays.*;
import static javax.lang.model.element.ElementKind.*;
import static javax.lang.model.element.Modifier.*;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.NestingKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.NoType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.TypeVisitor;

class Model {

	static TypeElement classElement(String name, Element... methods) {
		return typeElement(name, ElementKind.CLASS,noType(), methods);
	}
	
	static TypeElement interfaceElement(CharSequence name, Element... methods) {
		return interfaceElement(name, noType(), methods);
	}
	
	static TypeElement interfaceElement(CharSequence name, TypeMirror superInterface, Element... methods) {
		return typeElement(name, ElementKind.INTERFACE, superInterface, methods);
	}

	private static TypeElement typeElement(CharSequence name, ElementKind kind,
			TypeMirror superInterface, Element... methods) {
		return typeElement(name(name), kind, superInterface, methods);
	}
	
	static ExecutableElement methodExecutableElement(String name, DeclaredType returnType,
			VariableElement... parameters) {
		return methodExecutableElement(name(name), METHOD, returnType, EnumSet.of(STATIC, PUBLIC), new TypeParameterElement[0], parameters);
	}
	
	static ExecutableElement methodExecutableElement(String name, DeclaredType returnType,
			TypeParameterElement typeParameters) {
		return methodExecutableElement(name(name), METHOD, returnType, EnumSet.of(STATIC, PUBLIC),
				new TypeParameterElement[] { typeParameters }, new VariableElement[0]);
	}
	
	static ExecutableElement methodExecutableElement(final Name name, final ElementKind kind,
			final TypeMirror returnType, final EnumSet<Modifier> modifiers, final TypeParameterElement[] typeParameter, 
			final VariableElement[] parameters) {
		return new ExecutableElement() {
			@Override public Name getSimpleName() {
				return name;
			}
			
			@Override public TypeMirror getReturnType() {
				return returnType;
			}
			
			@Override public ElementKind getKind() { 
				return kind; 
			}
			
			@Override public Set<Modifier> getModifiers() { 
				return modifiers; 
			}
			
			@Override public List<? extends VariableElement> getParameters() {
				return asList(parameters);
			}
			@Override public List<? extends TypeParameterElement> getTypeParameters() { 
				return asList(typeParameter);
			}
			
			@Override
			public String toString() {
				String msg = "MethodExecutableElement[name=%s,kind=%s,returnType%s,modifiers=%s";
				return format(msg, name, kind, returnType, modifiers);
			}
			
			@Override public boolean equals(Object obj) { throw new UnsupportedOperationException(); }
			@Override public int hashCode() { throw new UnsupportedOperationException(); }
			@Override public AnnotationValue getDefaultValue() { throw new UnsupportedOperationException(); }
			@Override public List<? extends TypeMirror> getThrownTypes() { throw new UnsupportedOperationException(); }
			@Override public boolean isVarArgs() { throw new UnsupportedOperationException(); }
			@Override public <R, P> R accept(ElementVisitor<R, P> v, P p) { throw new UnsupportedOperationException(); }
			@Override public TypeMirror asType() { throw new UnsupportedOperationException(); }
			@Override public <A extends Annotation> A getAnnotation( Class<A> annotationType) { throw new UnsupportedOperationException(); }
			@Override public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
			@Override public List<? extends Element> getEnclosedElements() { throw new UnsupportedOperationException(); } 
			@Override public Element getEnclosingElement() { throw new UnsupportedOperationException(); } 
		        public boolean isDefault() { throw new UnsupportedOperationException(); }
		        public TypeMirror getReceiverType() { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		};
	}
	
	public static PrimitiveType primitiveType(){
		return new PrimitiveType(){

			@Override
			public <R, P> R accept(TypeVisitor<R, P> v, P p) { return v.visitPrimitive(this, p); }
		        public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
		        @Override public TypeKind getKind() { throw new UnsupportedOperationException(); }
		};
	}
	
	public static DeclaredType declaredType(final Element element, final TypeMirror... argumentTypes) {
		return new DeclaredType(){
			
			@Override public Element asElement() {
				return element;
			}
			
			@Override public List<? extends TypeMirror> getTypeArguments() {
				return Arrays.asList(argumentTypes);
			}
			
			@Override
			public String toString() {
				return format("DeclareTypeModel[element=%s, argumentTypes=%s]",
						element, Arrays.toString(argumentTypes));
			}
			
			@Override public <R, P> R accept(TypeVisitor<R, P> v, P p) { 
				return v.visitDeclared(this, p);
			}
			
			@Override public boolean equals(Object obj) { throw new UnsupportedOperationException(); }
			@Override public int hashCode() { throw new UnsupportedOperationException(); }

		        public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
		    
			@Override public TypeKind getKind() { throw new UnsupportedOperationException(); }
			@Override public TypeMirror getEnclosingType() { throw new UnsupportedOperationException(); }
			
		};
	}
	
	static Name name(final CharSequence content) {
		return new Name(){
			@Override
			public boolean contentEquals(CharSequence cs) {
				return cs.toString().equals(content.toString());
			}
			@Override public String toString(){
				return content == null ? "<null>" : content.toString();
			}
			
			@Override public int length(){
				return content.length();
			}

			@Override public char charAt(int index) { 
				return content.charAt(index);
			}
			
			@Override public CharSequence subSequence(int start, int end) { 
				return content.subSequence(start, end);
			}
			
			@Override public int hashCode() { throw new UnsupportedOperationException(); }
			
			
		};
	}
	
	private static TypeElement typeElement( final Name qualifiedName, final ElementKind elementKind, final TypeMirror superInterface, final Element[] enclosed){
		return new TypeElement(){
			
			@Override public ElementKind getKind() {
				return elementKind;
			}
			
			@Override public List<? extends Element> getEnclosedElements() { 
				return Arrays.asList(enclosed); 
			}
			@Override public Name getQualifiedName() { 
				return qualifiedName;
			}
			
			@Override public Name getSimpleName() {
				return qualifiedName;
			}
			
			@Override public TypeMirror getSuperclass() {
				return noType();
			}

			@Override public List<? extends TypeMirror> getInterfaces(){
				return Collections.singletonList(superInterface);
			}
			
			@Override
			public String toString(){
				return format("TypeElementModel[name=[%s], kind=[%s], enclosed=[%s]]", qualifiedName, elementKind, Arrays.toString(enclosed));
			}
			
			@Override public <R, P> R accept(ElementVisitor<R, P> v, P p) {
				return v.visitType(this, p);
			}
			
			@Override public int hashCode() { throw new UnsupportedOperationException(); }
			@Override public NestingKind getNestingKind() { throw new UnsupportedOperationException();  }
			@Override public List<? extends TypeParameterElement> getTypeParameters() { throw new UnsupportedOperationException(); }
			@Override public TypeMirror asType() { throw new UnsupportedOperationException(); }
			@Override public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
			@Override public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
			@Override public Element getEnclosingElement() { throw new UnsupportedOperationException(); }
			@Override public Set<Modifier> getModifiers() { throw new UnsupportedOperationException(); }
			
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		};
	}

	private static TypeMirror noType(){
		return new NoType() {
			@Override public <R, P> R accept(TypeVisitor<R, P> v, P p) { throw new UnsupportedOperationException(); }
			@Override public TypeKind getKind() { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
		};
	}

	static ArrayType arrayType(final TypeMirror type){
		return new ArrayType(){
			@Override public <R, P> R accept(TypeVisitor<R, P> v, P p) { return v.visitArray(this, p); }
			@Override public TypeMirror getComponentType() { return type; }

 			@Override public TypeKind getKind() { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
		};
	}
	
	static TypeVariable typeVariable(){
		return new TypeVariable() {
			@Override public TypeKind getKind() { throw new UnsupportedOperationException(); }
			@Override public <R, P> R accept(TypeVisitor<R, P> v, P p) { return v.visitTypeVariable(this, p);}
			@Override public TypeMirror getUpperBound() { throw new UnsupportedOperationException(); }
			@Override public TypeMirror getLowerBound() { throw new UnsupportedOperationException(); }
			@Override public Element asElement() { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
		};
	}
	
	static VariableElement variableElement(final CharSequence name, final TypeMirror type){
		return new VariableElement() {
			
			@Override
			public Name getSimpleName() {
				return name(name);
			}
			
			@Override
			public TypeMirror asType() {
				return type;
			}
			
			@Override public Set<Modifier> getModifiers() { throw new UnsupportedOperationException(); }
			@Override public ElementKind getKind() { throw new UnsupportedOperationException(); }
			@Override public Element getEnclosingElement() { throw new UnsupportedOperationException(); } 
			@Override public List<? extends Element> getEnclosedElements() { throw new UnsupportedOperationException(); }
		        public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        @Override public <R, P> R accept(ElementVisitor<R, P> v, P p) { throw new UnsupportedOperationException(); }
			@Override public Object getConstantValue() { throw new UnsupportedOperationException(); }
		};
	}
	
	static TypeParameterElement typeParameterElement() {
		return typeParameterElement(Collections.<TypeMirror> emptyList());
	}
	
	static TypeParameterElement typeParameterElement(final List<TypeMirror> bounds){
		return new TypeParameterElement(){
			@Override public List<? extends TypeMirror> getBounds() { 
				return bounds;
			} 
			
			@Override public Element getGenericElement() { throw new UnsupportedOperationException(); } 
			@Override public <R, P> R accept(ElementVisitor<R, P> v, P p) { throw new UnsupportedOperationException(); }
			@Override public TypeMirror asType() { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A getAnnotation(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType) { throw new UnsupportedOperationException(); }
		        public List<? extends AnnotationMirror> getAnnotationMirrors() { throw new UnsupportedOperationException(); }
		        @Override public List<? extends Element> getEnclosedElements() { throw new UnsupportedOperationException(); }
			@Override public Element getEnclosingElement() { throw new UnsupportedOperationException(); }
			@Override public ElementKind getKind() { throw new UnsupportedOperationException(); }
			@Override public Set<Modifier> getModifiers() { throw new UnsupportedOperationException(); }
			@Override public Name getSimpleName() { throw new UnsupportedOperationException(); } 
		};
	}
}
