/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import static net.java.quickcheck.generator.CombinedGeneratorsIterables.someLists;
import static net.java.quickcheck.generator.PrimitiveGenerators.integers;
import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.testng.annotations.Test;

public class SortedListTest {

	@Test
	public void sortedListCreation() {
		for (List<Integer> any : someLists(integers())) {
			SortedList sortedList = new SortedList(any);
			
			List<Integer> expected = sort(any);
			assertEquals(expected, sortedList.toList());
		}
	}

	private List<Integer> sort(List<Integer> any) {
		ArrayList<Integer> sorted = new ArrayList<Integer>(any);
		Collections.sort(sorted);
		return sorted;
	}
}