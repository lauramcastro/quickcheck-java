/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package deterministic;

import static net.java.quickcheck.generator.CombinedGenerators.uniqueValues;
import static net.java.quickcheck.generator.PrimitiveGenerators.integers;
import static org.junit.Assert.assertEquals;
import net.java.quickcheck.Generator;
import net.java.quickcheck.junit.SeedInfo;

import org.junit.Rule;
import org.junit.Test;

/**
 * Example using the seed info method rule to get and restore the seed to run 
 * a test deterministically. 
 */
public class SeedInfoExample {

	@Rule public SeedInfo seed = new SeedInfo();
	
	@Test public void run(){
		Generator<Integer> unique = uniqueValues(integers());
		assertEquals(unique.next(), unique.next());
	}
	
	//java.lang.AssertionError: expected:<243172514> but was:<-917691317> (Seed was 3084746326687106280L.)
	@Test public void restore(){
		seed.restore(3084746326687106280L);
		Generator<Integer> unique = uniqueValues(integers());
		assertEquals(unique.next(), unique.next());
	}
}
