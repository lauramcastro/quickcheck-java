/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import static net.java.quickcheck.QuickCheck.forAll;
import static net.java.quickcheck.generator.PrimitiveGeneratorsIterables.someIntegers;
import static org.testng.Assert.assertEquals;
import net.java.quickcheck.characteristic.AbstractCharacteristic;
import net.java.quickcheck.generator.PrimitiveGenerators;
import net.java.quickcheck.generator.iterable.Iterables;

import org.testng.annotations.Test;


public class DifferentQuickcheckRunnerStyleExamplesTest {

    @Test
    public void classic() {
        forAll(PrimitiveGenerators.integers(), new AbstractCharacteristic<Integer>() {

            @Override
            protected void doSpecify(Integer any) throws Throwable {
                assertEquals(any * 2, any + any);
            }
        });
    }

    @Test
    public void iterable() {
        for (Integer any : Iterables.toIterable(PrimitiveGenerators.integers())) {
            assertEquals(any * 2, any + any);
        }
    }
    
    @Test
    public void iterableGeneratedAdapter() {
        for (Integer any : someIntegers()) {
            assertEquals(any * 2, any + any);
        }
    }
}