/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import static net.java.quickcheck.QuickCheck.forAll;
import static net.java.quickcheck.generator.CombinedGenerators.nonEmptyLists;
import static net.java.quickcheck.generator.PrimitiveGenerators.fixedValues;
import static net.java.quickcheck.generator.PrimitiveGenerators.letterStrings;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.util.List;

import net.java.quickcheck.Generator;
import net.java.quickcheck.characteristic.AbstractCharacteristic;

import org.testng.annotations.Test;

public class FileGeneratorTest {

	@Test
	public void file() {
		forAll(new FileGenerator(), new AbstractCharacteristic<File>() {
			@Override
			protected void doSpecify(File any) {
				assertTrue(any.isAbsolute());
			}
		});
	}

	class FileGenerator implements Generator<File> {

		Generator<File> roots = fixedValues(File.listRoots());
		Generator<List<String>> paths = nonEmptyLists(letterStrings());

		@Override
		public File next() {
			File f = roots.next();
			for (String p : paths.next()) {
				f = new File(f, p);
			}
			return f;
		}
	}
}
