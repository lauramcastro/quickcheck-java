package splitter;

import static com.google.common.primitives.Chars.asList;
import static net.java.quickcheck.generator.CombinedGenerators.excludeValues;
import static net.java.quickcheck.generator.CombinedGeneratorsIterables.someNonEmptyLists;
import static net.java.quickcheck.generator.PrimitiveGenerators.characters;
import static net.java.quickcheck.generator.PrimitiveGenerators.strings;
import static org.testng.Assert.assertEquals;

import java.util.List;

import org.testng.annotations.Test;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class QuickcheckSplitterTest {

	@Test public void simpleSplit() {
		for (List<String> words : someNonEmptyLists(strings())) {
			char separator = anyDistinctCharacter(words);
			String input = Joiner.on(separator).join(words);
			Iterable<String> letters = Splitter.on(separator).split(input);
			assertEquals(words, Lists.newArrayList(letters));
		}
	}

	private char anyDistinctCharacter(List<String> words) {
		char[] notAllowedAsSeperator = Joiner.on("").join(words).toCharArray();
		return excludeValues(characters(), asList(notAllowedAsSeperator)).next();
	}
}