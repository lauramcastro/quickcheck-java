package splitter;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class DataProviderSplitterTest {

	static final String SPLIT = "split";
	
	@DataProvider(name = SPLIT)
	public Object[][] split() {
		Object[] simpleSplit = {
				"a,b,c", ",", new String[] { "a", "b", "c" } };
		Object[] splitWithDoubleDelimiter = {
				"a,,b,c", ",", new String[] { "a", "", "b", "c" } };
		Object[] splitWithDoubleDelimiterAndSpace = {
				"a,, b,c", ",", new String[] { "a", "", " b", "c" } };
		Object[] splitWithTrailingDelimiter = {
				"a,b,c,", ",", new String[] { "a", "b", "c", "" } };
		Object[] splitWithLeadingDelimiter = {
				",a,b,c", ",", new String[] { "", "a", "b", "c" } };
		Object[] splitWithMulitpleLetters = {
				"Testing-rocks-Debugging-sucks", "-", new String[] { "Testing", "rocks", "Debugging", "sucks" } };
		return new Object[][] { simpleSplit, splitWithDoubleDelimiter, splitWithDoubleDelimiterAndSpace, 
				splitWithTrailingDelimiter, splitWithLeadingDelimiter, splitWithMulitpleLetters };
	}

	@Test(dataProvider = SPLIT)
	public void simpleSplit(String simple, String separator, String[] expected) {
		Iterable<String> letters = Splitter.on(separator).split(simple);
		assertEquals(Arrays.asList(expected), Lists.newArrayList(letters));
	}
}
