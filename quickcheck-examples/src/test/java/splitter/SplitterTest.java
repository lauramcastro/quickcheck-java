package splitter;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;

import org.testng.annotations.Test;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class SplitterTest {
	
	@Test public void testCharacterSimpleSplit() {
		String simple = "a,b,c";
		Iterable<String> letters = Splitter.on(',').split(simple);
		assertContentsInOrder(letters, "a", "b", "c");
	}
	@Test public void testCharacterSplitWithDoubleDelimiter() {
		String doubled = "a,,b,c";
		Iterable<String> letters = Splitter.on(',').split(doubled);
		assertContentsInOrder(letters, "a", "", "b", "c");
	}
	@Test public void testCharacterSplitWithDoubleDelimiterAndSpace() {
		String doubled = "a,, b,c";
		Iterable<String> letters = Splitter.on(',').split(doubled);
		assertContentsInOrder(letters, "a", "", " b", "c");
	}
	@Test public void testCharacterSplitWithTrailingDelimiter() {
		String trailing = "a,b,c,";
		Iterable<String> letters = Splitter.on(',').split(trailing);
		assertContentsInOrder(letters, "a", "b", "c", "");
	}
	@Test public void testCharacterSplitWithLeadingDelimiter() {
		String leading = ",a,b,c";
		Iterable<String> letters = Splitter.on(',').split(leading);
		assertContentsInOrder(letters, "", "a", "b", "c");
	}
	@Test public void testCharacterSplitWithMulitpleLetters() {
		Iterable<String> testCharacteringMotto = Splitter.on('-').split(
				"Testing-rocks-Debugging-sucks");
		assertContentsInOrder(testCharacteringMotto, "Testing", "rocks",
				"Debugging", "sucks");
	}
	private void assertContentsInOrder(Iterable<String> actual,
			String... expected) {
		assertEquals(Arrays.asList(expected), Lists.newArrayList(actual));
	}
}
