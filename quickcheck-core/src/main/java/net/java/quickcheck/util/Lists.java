/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Lists {

	public static <T> List<T> toList(Iterable<T> values) {
		Assert.notNull(values, "values");
		if(values instanceof Collection<?>){
			return new ArrayList<T>((Collection<T>) values);
		}
		ArrayList<T> list = new ArrayList<T>();
		for(T t : values) list.add(t);
		return list;
	}
}
