/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import java.util.Collection;

import net.java.quickcheck.Generator;
import net.java.quickcheck.util.Assert;
import net.java.quickcheck.util.Lists;

public class ExcludingGenerator<T> extends VetoableGenerator<T>{

	private final Collection<T> excluded;

	public ExcludingGenerator(Generator<T> generator, Iterable<T> excluded, int tries) {
		super(generator, tries);
		Assert.notNull(excluded, "excluded");
		this.excluded = Lists.toList(excluded);
	}

	@Override
	protected boolean tryValue(T value) {
		return !excluded.contains(value);
	}

}
