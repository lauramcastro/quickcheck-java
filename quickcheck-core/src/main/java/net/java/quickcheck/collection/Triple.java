/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.collection;

import static java.lang.String.format;

/**
 * A tuple of three values.
 * 
 * @param <A>
 *            type of first entry
 * @param <B>
 *            type of second entry
 * @param <C>
 *            type of third entry
 * 
 */
public class Triple<A, B, C> extends Pair<A, B> {

	private final C third;

	@Override
	public int hashCode() {
		final int prime = 31;
		return prime * super.hashCode() + ((third == null) ? 0 : third.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj) || getClass() != obj.getClass()) return false;
		Triple<?,?,?> other = (Triple<?, ?, ?>) obj;
		if(!super.equals(obj)) return false;
		if(third == null) {
			if(other.third != null) return false;
		} else if(!third.equals(other.third)) return false;
		return true;
	}

	public Triple(A first, B second, C third) {
		super(first, second);
		this.third = third;
	}

	public C getThird() {
		return third;
	}
	
	@Override
	public String toString() {
		return format("[%s,%s,%s]", getFirst(), getSecond(), getThird());
	}
}
