/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.junit;

import static java.lang.String.*;
import net.java.quickcheck.generator.distribution.RandomConfiguration;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

/**
 * {@link SeedInfo} adds information about the seed used by the random number.
 * 
 * <p>The information is added to the {@link AssertionError} thrown for failed tests.</p>
 * 
 * <p>You can used the
 * {@link #restore(long)} method to rerun a test with the same seed.
 * </p>
 * <p>
 * Alternatively you can use {@link RandomConfiguration} directly in your tests.
 * </p>
 */
public class SeedInfo implements MethodRule{

	private long seed;
	boolean restored;
	
	/**
	 * Set the seed to a value logged before.
	 */
	public void restore(long seed){
		RandomConfiguration.setSeed(seed);
		this.seed = seed;
		this.restored = true;
	}
	
	@Override public Statement apply(final Statement base, FrameworkMethod frameworkMethod, Object target) {
		if(!restored) seed = RandomConfiguration.initSeed();
		restored = false;
		return new Statement() {
			@Override public void evaluate() throws Throwable {
				try {
					base.evaluate();
				} catch(Throwable e) {
					String message = e.getMessage() == null ? "" : e.getMessage();
					AssertionError error = new AssertionError(format("%s (Seed was %sL.)", message, seed));
					error.setStackTrace(e.getStackTrace());
					throw error;
				}
			}
		};
	}
}