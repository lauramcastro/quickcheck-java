/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.characteristic;

import static java.lang.String.*;
import static java.util.Collections.*;
import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.CombinedGeneratorsIterables.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import net.java.quickcheck.Generator;
import net.java.quickcheck.StatefulGenerator;

import org.junit.Before;
import org.junit.Test;

public class ClassificationTest {

	private String category;
	private String otherCategory;

	@Before
	public void setUp() throws Exception {
		category = strings().next();
		otherCategory = strings().next();
	}

	@Test
	public void testToStringClassification() throws Exception {
		Classification classification = new Classification();
		classification.doClassify(true, category);
		classification.call();
		assertEquals(format("Classifications :\n%s = %1.2f%%", category, 100d), classification.toString());
	}

	@Test
	public void testClassifyWithPredicateFalse() throws Exception {
		Classification classification = new Classification();
		classification.doClassify(false, "cat");
		classification.call();
		assertEquals(EMPTY_LIST, classification.getCategories());
	}

	@Test
	public void testClassifyMultipleValues() throws Exception {
		String[] categories = { category, category, category, otherCategory };
		Classification classification = new Classification();
		for (String cat : categories) classification.classifyCall(cat);
		assertEquals(75.0, classification.getFrequency(category), 0);
		assertEquals(25.0, classification.getFrequency(otherCategory), 0);
	}

	@Test
	public void testCategoriesAreSortedByFrequency() {
		StatefulGenerator<Integer> uniqueInts = uniqueValues(integers(1, DEFAULT_COLLECTION_MAX_SIZE));
		for(List<Integer> frequencies : someSortedLists(uniqueInts)) {
			uniqueInts.reset();
			reverse(frequencies);
			Generator<String> names = uniqueValues(strings());
			List<Object> expected = new ArrayList<Object>();
			Classification classification = new Classification();
			for(Integer frequency : frequencies) {
				String name = names.next();
				for(int i=0;i<frequency;i++) classification.classifyCall(name);
				expected.add(name);
			}
			assertEquals(expected, classification.getCategories());
		}
	}

	@Test
	public void testDoNotClassifyAfterGetCategories() {
		Classification classification = new Classification();
		classification.getCategories();
		assertClassifyFails(classification);
	}

	@Test
	public void testDoNotClassifyAfterGetFrequency() {
		Classification classification = new Classification();
		classification.getFrequency(new Object());
		assertClassifyFails(classification);
	}

	@Test
	public void testDoNotClassifyAfterToString() {
		Classification classification = new Classification();
		classification.toString();
		assertClassifyFails(classification);
	}

	private void assertClassifyFails(Classification classification) {
		try {
			classification.doClassify(true, "");
			fail();
		} catch (IllegalStateException e) {
		}
		try {
			classification.call();
			fail();
		} catch (IllegalStateException e) {
		}
	}

	@Test
	public void testUnknownClassificationIsEmpty() {
		assertEquals(0.0, new Classification().getFrequency(strings().next()), 0.0);
		assertTrue(new Classification().getCategories().isEmpty());
	}
}
