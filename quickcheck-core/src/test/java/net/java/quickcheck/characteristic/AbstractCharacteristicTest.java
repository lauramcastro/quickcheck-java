/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.characteristic;

import static java.util.Collections.EMPTY_LIST;
import junit.framework.TestCase;

public class AbstractCharacteristicTest extends TestCase {

	private AbstractCharacteristic<Integer> characteristic;

	@Override
	protected void setUp() throws Exception {
		characteristic = new AbstractCharacteristic<Integer>() {
			@Override
			protected void doSpecify(Integer any) {
			}
		};
	}

	public void testClassifyEmpty() {
		assertEquals(EMPTY_LIST, characteristic.getClassification()
				.getCategories());
	}

	public void testToClassifiedString() {
		assertEquals("Classifications :none", characteristic
				.getClassification().toString());
	}

	public void testClassify() throws Throwable {
		String classification = "string";
		characteristic.classify(classification);
		expectOneClassificationEntry(classification);
	}

	private void expectOneClassificationEntry(String classification)
			throws Throwable {
		characteristic.specify(0);
		assertEquals(100.0, characteristic.getClassification().getFrequency(
				classification));
	}

}
