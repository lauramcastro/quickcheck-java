/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.junit;

import static java.lang.String.*;
import static net.java.quickcheck.generator.PrimitiveGeneratorSamples.*;
import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runners.model.Statement;

public class SeedInfoTest {

	@Test public void happyPath() throws Throwable{
		SeedInfo seedInfo = new SeedInfo();
		seedInfo.apply(noop(), null, null).evaluate();
	}

	@Test public void exceptionThrown() throws Throwable{
		SeedInfo seedInfo = new SeedInfo();
		final Exception thrown = new Exception(anyString());
		try {
			seedInfo.apply(exceptionThrown(thrown), null, null).evaluate();
		} catch(AssertionError e) {
			assertTrue(e.getMessage().matches(format("(?i).*seed.*\\d+L.*")));
			assertTrue(e.getMessage().startsWith(thrown.getMessage()));
			assertTrue(Arrays.deepEquals(e.getStackTrace(), thrown.getStackTrace()));
		}
	}

	@Test public void restoreHappyPath() throws Throwable{
		Long seed = anyLong();
		SeedInfo seedInfo = new SeedInfo();
		seedInfo.restore(seed);
		seedInfo.apply(noop(), null, null).evaluate();
		assertFalse(seedInfo.restored);
	}
	
	@Test public void restoreExceptionThrown() throws Throwable{
		Long seed = anyLong();
		SeedInfo seedInfo = new SeedInfo();
		final Exception thrown = new Exception();
		seedInfo.restore(seed);
		try {
			seedInfo.apply(exceptionThrown(thrown), null, null).evaluate();
		} catch(AssertionError e) {
			assertTrue(e.getMessage().contains(Long.toString(seed)));
		}
		assertFalse(seedInfo.restored);
	}
	
	private Statement noop() {
		return new Statement() { @Override public void evaluate(){ } };
	}
	
	private Statement exceptionThrown(final Exception thrown) {
		return new Statement() {
			@Override public void evaluate() throws Throwable{
				throw thrown;
			}
		};
	}
}
