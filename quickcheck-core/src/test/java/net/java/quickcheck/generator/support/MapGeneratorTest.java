/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGeneratorSamples.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.hamcrest.core.IsInstanceOf.*;
import static org.junit.Assert.*;

import java.util.Map;
import java.util.Map.Entry;

import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.CombinedGenerators;

import org.junit.Test;


public class MapGeneratorTest {

	@Test public void maps(){
		Generator<Map<String, Long>> maps = CombinedGenerators.maps(new StringGenerator(), new LongGenerator());
		Map<String, Long> next = maps.next();
		assertTrue(next.size() <= DEFAULT_COLLECTION_MAX_SIZE);
		for(Entry<String, Long> e : next.entrySet()) {
			assertThat(e.getKey(), instanceOf(String.class));
			assertThat(e.getValue(), instanceOf(Long.class));
		}
	}
	
	@Test public void mapsWithSize(){
		int size = anyPositiveInteger(DEFAULT_COLLECTION_MAX_SIZE);
		Generator<Map<String, Long>> maps = CombinedGenerators.maps(new StringGenerator(), new LongGenerator(),
				fixedValues(size));
		assertEquals(size, maps.next().size());
	}
}
