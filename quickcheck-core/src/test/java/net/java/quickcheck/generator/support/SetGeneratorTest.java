/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.QuickCheck.*;
import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.support.SetGenerator.*;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsInstanceOf.*;
import static org.junit.Assert.*;
import static org.hamcrest.core.IsCollectionContaining.*;

import java.util.Collection;
import java.util.Set;

import net.java.quickcheck.Generator;
import net.java.quickcheck.characteristic.AbstractCharacteristic;
import net.java.quickcheck.generator.CombinedGenerators;
import net.java.quickcheck.generator.PrimitiveGenerators;
import net.java.quickcheck.generator.distribution.Distribution;

import org.junit.Test;

public class SetGeneratorTest extends AbstractCollectionTestCase {

	@Test public void typeVariance(){
		Generator<Set<Object>> objs = CombinedGenerators.<Object> nonEmptySets(integers());
		assertThat(objs.next(), hasItem(is(instanceOf(Integer.class))));
	}
	
	@Override
	protected Generator<Collection<Integer>> defaultGenerator() {
		return cast(CombinedGenerators.sets(PrimitiveGenerators.integers()));
	}

	@Override
	protected Generator<Collection<Integer>> normalDistributionGenerator() {
		return cast(CombinedGenerators.sets(integers(), integers(0, MAX_SIZE, Distribution.POSITIV_NORMAL)));
	}

	@Test
	public void testSizeIsValid() throws Exception {
		forAll(sets(integers(0, MAX_SIZE * 10), MAX_SIZE, MAX_SIZE), new AbstractCharacteristic<Set<Integer>>() {
			@Override
			protected void doSpecify(Set<Integer> any) throws Throwable {
				assertEquals(MAX_SIZE, any.size());
			}
		});
	}

	@Override
	protected Generator<Collection<Integer>> nonEmpty() {
		return cast(CombinedGenerators.nonEmptySets(integers()));
	}
}