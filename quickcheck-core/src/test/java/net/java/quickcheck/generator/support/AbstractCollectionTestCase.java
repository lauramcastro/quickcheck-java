/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static java.lang.String.*;
import static net.java.quickcheck.QuickCheck.*;
import static net.java.quickcheck.characteristic.ClassificationTestHelper.*;
import static net.java.quickcheck.generator.support.ListGenerator.*;
import static org.junit.Assert.*;

import java.util.Collection;

import net.java.quickcheck.Generator;
import net.java.quickcheck.characteristic.AbstractCharacteristic;
import net.java.quickcheck.characteristic.Classification;
import net.java.quickcheck.generator.Generators;

import org.junit.Test;

public abstract class AbstractCollectionTestCase {

	private static final String UPPER_HALF = "upperHalf";
	private static final String LOWER_HALF = "lowerHalf";

	@Test
	public void testGeneratorPositiveNormalLength() {
		Generator<Collection<Integer>> listsGenerator = normalDistributionGenerator();
		double expectedLowerHalfFrequency = 60.0;
		double expectedUpperHalfFrequency = 10.0;
		testCollectionGenerator(listsGenerator, expectedLowerHalfFrequency,
				expectedUpperHalfFrequency);
	}

	protected abstract Generator<Collection<Integer>> normalDistributionGenerator();

	@Test
	public void testGenerator() {
		double expectedLowerHalfFrequency = 40.0;
		double expectedUpperHalfFrequency = 40.0;
		Generator<Collection<Integer>> collectionGenerator = defaultGenerator();
		testCollectionGenerator(collectionGenerator,
				expectedLowerHalfFrequency, expectedUpperHalfFrequency);
	}

	protected abstract Generator<Collection<Integer>> defaultGenerator();

	void testCollectionGenerator(
			Generator<Collection<Integer>> generator,
			double expectedLowerHalfFrequency, double expectedUpperHalfFrequency) {
		testCollectionGenerator(generator, expectedLowerHalfFrequency,
				expectedUpperHalfFrequency, MIN_SIZE, MAX_SIZE);
	}

	void testCollectionGenerator(
			Generator<Collection<Integer>> listGenerator,
			double expectedLowerHalfFrequency,
			double expectedUpperHalfFrequency, final int lo, final int hi) {
		final int half = (hi - lo) / 2 + lo;
		AbstractCharacteristic<Collection<Integer>> characteristic = new AbstractCharacteristic<Collection<Integer>>() {
			@Override
			public void doSpecify(Collection<Integer> any) {
				String msg = format("size was %s", any.size());
				assertTrue(msg, any.size() <= hi);
				assertTrue(msg, any.size() >= lo);
				for (Integer e : any) {
					assertNotNull(e);
				}
				classify(any.size() <= half, LOWER_HALF);
				classify(any.size() >= half, UPPER_HALF);
			}
		};
		forAll(500, listGenerator, characteristic);
		Classification classification = characteristic.getClassification();
		assertFrequencyGreater(classification, expectedLowerHalfFrequency, LOWER_HALF);
		assertFrequencyGreater(classification, expectedUpperHalfFrequency, UPPER_HALF);
	}

	@Test public void testNonEmptyList() {
		forAll(nonEmpty(), new AbstractCharacteristic<Collection<Integer>>() {
 
			@Override
			protected void doSpecify(Collection<Integer> any) throws Throwable {
				assertFalse(any.isEmpty());
				assertTrue(any.size() <= ListGenerator.MAX_SIZE);
			}
		});
	}

	protected abstract Generator<Collection<Integer>> nonEmpty();
	
	<T extends Collection<Integer>> Generator<Collection<Integer>> cast(Generator<T> generator) {
		return Generators.<Collection<Integer>> cast(generator);
	}
}
