/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator;

import static java.util.Arrays.*;
import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.iterable.Iterables.*;
import static org.hamcrest.collection.IsEmptyCollection.*;
import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import net.java.quickcheck.Generator;
import net.java.quickcheck.characteristic.Classification;
import net.java.quickcheck.collection.Pair;
import net.java.quickcheck.collection.Triple;

import org.junit.Test;

public class CombinedGeneratorsTest {

	private static final String NULL = "null";

	@Test public void frequencyNotValidWithFrequency0() {
		try {
			frequency(nulls(), 0);
		} catch(IllegalArgumentException e) {}
	}

	@Test public void frequencyTrivial() throws Throwable {
		Classification classification = new Classification();
		for(Integer i : toIterable(frequency(PrimitiveGenerators.<Integer> nulls(), 1))) {
			classification.classifyCall(i == null, NULL);
		}
		assertEquals(100, classification.getFrequency(NULL), 0);
	}

	@Test public void frequencyNullAndIntegerEqualDistribution() throws Throwable {
		Generator<Integer> nulls = nulls();
		Classification classification = new Classification();
		for(Integer i : toIterable(frequency(nulls, 1).add(integers(), 1))) {
			classification.classifyCall(i == null, NULL);
		}
		assertEqualDistribution(classification.getFrequency(NULL));
	}

	@Test public void frequency3NullAnd1Integer() throws Throwable {
		Generator<Integer> nulls = nulls();
		Classification classification = new Classification();
		for(Integer i : toIterable(frequency(nulls, 3).add(integers(), 1))) {
			classification.classifyCall(i == null, NULL);
		}
		double nullFrequency = classification.getFrequency(NULL);
		assertTrue("" + nullFrequency, nullFrequency > 50 && nullFrequency < 90);
	}

	@Test public void oneOf() throws Throwable {
		Generator<Integer> nulls = nulls();
		Generator<Integer> gen = CombinedGenerators.oneOf(nulls).add(integers());
		Classification classification = new Classification();
		for(Integer i : toIterable(gen)) {
			classification.classifyCall(i == null, NULL);
		}
		assertEqualDistribution(classification.getFrequency(NULL));
	}

	@Test public void vectorEmpty() {
		Generator<List<Integer>> gen = vectors(PrimitiveGenerators.integers(), 0);
		List<Integer> next = gen.next();
		assertTrue(next.isEmpty());
	}

	@Test public void vector() {
		Generator<List<Integer>> gen = vectors(PrimitiveGenerators.integers(), 3);
		List<Integer> actual = gen.next();
		assertEquals(3, actual.size());
		for(Integer i : actual) {
			assertNotNull(i);
		}
	}

	@Test public void pair() {

		Object returnByFirst = new Object();
		Object returnBySecond = new Object();

		Generator<Pair<Object, Object>> pairs = pairs(fixedValues(returnByFirst), fixedValues(returnBySecond));

		for(Pair<Object, Object> p : toIterable(pairs)) {
			assertSame(returnByFirst, p.getFirst());
			assertSame(returnBySecond, p.getSecond());
		}
	}
	
	@Test public void sortedPair() {
		Generator<Pair<Integer, Integer>> pairs = CombinedGenerators.sortedPairs(integers());
		for(Pair<Integer, Integer> pair : toIterable(pairs)) {
			assertTrue(pair.getFirst().compareTo(pair.getSecond()) <= 0);
		}
	}

	@Test public void triple() {
		Object returnByFirst = new Object();
		Object returnBySecond = new Object();
		Object returnByThird = new Object();

		Generator<Triple<Object, Object, Object>> triples = CombinedGenerators.triples(fixedValues(returnByFirst),
				fixedValues(returnBySecond), fixedValues(returnByThird));

		for(Triple<Object, Object, Object> triple : toIterable(triples)) {
			assertSame(returnByFirst, triple.getFirst());
			assertSame(returnBySecond, triple.getSecond());
			assertSame(returnByThird, triple.getThird());
		}
	}
	
	@Test public void sortedTriple() {
		Generator<Triple<Integer, Integer, Integer>> pairs = CombinedGenerators.sortedTriple(integers());
		for(Triple<Integer, Integer, Integer> triple : toIterable(pairs)) {
			assertTrue(triple.getFirst().compareTo(triple.getSecond()) <= 0);
			assertTrue(triple.getSecond().compareTo(triple.getThird()) <= 0);
		}
	}

	@Test public void nullsAnd() {
		Classification classification = new Classification();
		for(Integer i : toIterable(CombinedGenerators.nullsAnd(integers()))) {
			classification.classifyCall(i == null, NULL);
		}
		double nullFrequency = classification.getFrequency(NULL);
		assertTrue(nullFrequency > 5 && nullFrequency < 65);
	}
	
	@Test public void testNullsAndWithVargsParameter(){
		List<Object> expected = new ArrayList<Object>(asList(new Object(), new Object(), null));
		Generator<Object> generator = CombinedGenerators.nullsAnd(expected.get(0), expected.get(1));
		for(int i = 0; i < 1 << 10 && expected.size() > 0; i++) {
			expected.remove(generator.next());
		}
		assertThat(expected, is(empty()));
	}

	private void assertEqualDistribution(double frequency) {
		assertTrue(frequency > 30 && frequency < 80);
	}
}