/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.characteristic.ClassificationTestHelper.*;
import static org.junit.Assert.*;
import net.java.quickcheck.QuickCheck;
import net.java.quickcheck.characteristic.AbstractCharacteristic;
import net.java.quickcheck.generator.distribution.Distribution;

import org.junit.Test;

public abstract class WholeNumberGeneratorTestCase<T extends Number> extends
		NumberGeneratorTestCase<T> {

	@Test
	public void testDistributionOfBoundsValues() {

		AbstractCharacteristic<T> characteristic = new AbstractCharacteristic<T>() {
			@Override
			protected void doSpecify(T any) {
				classify(any.longValue());
			}
		};
		byte high = 1;
		byte low = -1;
		QuickCheck.forAll(1000, generator(low, high, Distribution.UNIFORM),
				characteristic);

		assertEquals(3, characteristic.getClassification().getCategories() .size());
		assertFrequencyGreater(characteristic.getClassification(), 28.0, -1L, 0L, 1L);
	}

}
