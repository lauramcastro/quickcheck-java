/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.iterable.Iterables.*;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsCollectionContaining.*;
import static org.hamcrest.core.IsNot.*;
import static org.junit.Assert.*;

import java.util.List;

import net.java.quickcheck.Generator;
import net.java.quickcheck.GeneratorException;
import net.java.quickcheck.generator.CombinedGenerators;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;


public class ExcludingGeneratorTest {

	@Test public void excludeNothing(){
		Integer value = integers().next();
		Generator<Integer> generator = CombinedGenerators.excludeValues(PrimitiveGenerators.fixedValues(value));
		assertEquals(value, generator.next());
	}
	
	@Test(expected=GeneratorException.class)
	public void excludeEverything(){
		Integer value = integers().next();
		CombinedGenerators.excludeValues(fixedValues(value), value).next();
	}
	
	@Test(expected=GeneratorException.class)
	public void excludeEverythingVargs(){
		Integer value = integers().next();
		CombinedGenerators.excludeValues(fixedValues(value), new Integer[]{value}).next();
	}
	
	@Test public void excludeCollection(){
		for(List<Integer> excluded : toIterable(lists(integers()))){
			Integer value = CombinedGenerators.excludeValues(integers(), excluded).next();
			assertThat(excluded, not(hasItem(value)));
		}
	}
	
	@Test public void excludeFixedValueGenerator(){
		for(List<Integer> fixedValues : toIterable(lists(integers(), 2))){
			Integer excluded = fixedValues(fixedValues).next();
			Integer value = CombinedGenerators.excludeValues(fixedValues, excluded).next();
			assertThat(value, is(not(excluded)));
		}
	}
}
