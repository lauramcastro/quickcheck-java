/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static java.lang.String.*;
import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.iterable.Iterables.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import net.java.quickcheck.Generator;
import net.java.quickcheck.StatefulGenerator;
import net.java.quickcheck.generator.CombinedGenerators;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;

public class UniqueComparableValuesGeneratorTest extends UniqueValuesGeneratorTest {
	
	@Test public void uniqueValues(){
		Generator<String> values = strings(1,1);
		StatefulGenerator<String> uniqueValues = CombinedGenerators.uniqueValues(values, CASE_INSENSITIVE_ORDER);
		for(List<String> gs : toIterable(lists(uniqueValues))){
			for(String g : gs) {
				for(String o : removed(gs, g)) {
					assertTrue(o + " " + g, CASE_INSENSITIVE_ORDER.compare(g, o) != 0);
				}
			}
			uniqueValues.reset();
		}
	}
	
	@Test public void unqueValuesContravariantComparator(){
		Comparator<Number> comparator = new Comparator<Number>(){
			@Override public int compare(Number arg0, Number arg1) {
				return Integer.valueOf(arg0.intValue()).compareTo(Integer.valueOf(arg1.intValue()));
			}
		};
		assertNotNull(CombinedGenerators.uniqueValues(PrimitiveGenerators.integers(), comparator).next());
	}

	Comparator<Boolean> naturalOrder = new Comparator<Boolean>() {
		@Override public int compare(Boolean o1, Boolean o2) { return o1.compareTo(o2); } 
	};
	@Override StatefulGenerator<Boolean> uniqueValuesGenerator(int tries, Generator<Boolean> generator) {
		return CombinedGenerators.uniqueValues(generator, naturalOrder, tries);
	}

	@Override StatefulGenerator<Boolean> uniqueValuesGenerator(Generator<Boolean> generator) {
		return CombinedGenerators.uniqueValues(generator, naturalOrder);
	}

	private List<String> removed(List<String> gs, String g) {
		List<String> others = new ArrayList<String>(gs);
		others.remove(g);
		return others;
	}
}
