/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.junit.Assert.*;
import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.PrimitiveGenerators;
import net.java.quickcheck.generator.iterable.Iterables;

import org.junit.Assert;
import org.junit.Test;

public class SubstringGeneratorTest {

	@Test public void areSubstrings() {
		for(String superstring : Iterables.toIterable(new StringGenerator())) {
			Generator<String> subs = PrimitiveGenerators.substrings(superstring);
			for(String sub : Iterables.toIterable(subs)) {
				Assert.assertTrue(superstring.contains(sub));
			}
		}
	}
	
	@Test public void ofSize() {
		for(String superstring : Iterables.toIterable(new StringGenerator())) {
			int size = new IntegerGenerator(0, superstring.length()).next();
			Generator<String> subs = PrimitiveGenerators.substrings(superstring, size);
			for(String sub : Iterables.toIterable(subs)) {
				assertTrue(superstring.contains(sub));
				assertEquals(size, sub.length());
			}
		}
	}
	
	@Test public void full() {
		String base = new StringGenerator().next();
		assertEquals(base, PrimitiveGenerators.substrings(base, base.length()).next());
	}
	
	@Test public void invalidMinSize() {
		String base = new StringGenerator().next();
		int invalidMin = new IntegerGenerator(Integer.MIN_VALUE, -1).nextInt();
		try {
			substrings(base, invalidMin, base.length()).next();
			fail();
		}catch(IllegalArgumentException e) { }
	}
	
	@Test public void invalidMaxSize() {
		String base = new StringGenerator().next();
		int invalidMax = new IntegerGenerator(base.length() + 1, Integer.MAX_VALUE).nextInt();
		try {
			substrings(base, 0, invalidMax).next();
			fail();
		}catch(IllegalArgumentException e) { }
	}
	
	@Test public void sizeInRange() {
		String base = new StringGenerator().next();
		int min = new IntegerGenerator(0, base.length()).nextInt();
		int max = new IntegerGenerator(min, base.length()).nextInt();
		for(String s : Iterables.toIterable(PrimitiveGenerators.substrings(base, min, max))){
			assertTrue(min <= s.length() && s.length() <= max);
			assertTrue(base.contains(s));
		}
	}
}
