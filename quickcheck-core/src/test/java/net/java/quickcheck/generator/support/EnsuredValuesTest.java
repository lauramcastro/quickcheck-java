/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static java.util.Arrays.*;
import static java.util.Collections.*;
import static org.hamcrest.core.IsCollectionContaining.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.java.quickcheck.Generator;
import net.java.quickcheck.StatefulGenerator;
import net.java.quickcheck.generator.CombinedGenerators;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;

public class EnsuredValuesTest {

	private static final String[] STRING_VALUES = { "a", "b", "c", "d", "e", "f", "g", "h" };
	private static final String DISJUNCT_STRING = "z";
	private static final Set<String> VALUES = 
		unmodifiableSet(new HashSet<String>(asList(STRING_VALUES)));
	static{
		assert ! VALUES.contains(DISJUNCT_STRING);
	}

	@Test public void rejectConstructionForEmptyIterable() {
		try {
			CombinedGenerators.ensureValues();
			fail("Creation with an empty collection is not permitted");
		} catch (Exception e) { }
	}

	@Test public void generateEachValueOneTimeInASequence() {
		assertGenerateEachValueOnce(CombinedGenerators.ensureValues(VALUES));
	}

	@Test public void generateEachValueOneTimeInASequenceArray() {
		assertGenerateEachValueOnce(CombinedGenerators.ensureValues(STRING_VALUES));
	}

	private void assertGenerateEachValueOnce(Generator<String> generator) {
		Set<String> rest = new HashSet<String>(VALUES);
		for(int idx = 0; idx < VALUES.size(); idx++) {
			assertTrue(rest.remove(generator.next()));
		}
		assertTrue(rest.isEmpty());
	}

	@Test public void generateRandomCollectionValuesAfterFirstSequence() {
		Generator<String> generator = CombinedGenerators.ensureValues(new HashSet<String>(VALUES));
		for (int idx = 0; idx < VALUES.size(); idx++) generator.next();
		for (int idx = 0; idx < VALUES.size() * 4; idx++) 
			assertThat(VALUES, hasItem(generator.next()));
	}

	@Test public void returnGeneratorValuesAfterFirstSequence() {
		int len = 2;
		Generator<String> strings = new StringGenerator(
				new FixedValuesGenerator<Integer>(len),
				new CharacterGenerator());
		Generator<String> generator = CombinedGenerators.ensureValues(
				new HashSet<String>(VALUES), strings);
		for (int idx = 0; idx < VALUES.size(); idx++) {
			assertFalse(generator.next().length() == len);
		}
		for (int idx = 0; idx < VALUES.size() * 4; idx++) {
			assertTrue(generator.next().length() == len);
		}
	}

	@Test public void resetGenerator() {
		StatefulGenerator<Boolean> generator = CombinedGenerators.ensureValues(
				Collections.singleton(true), PrimitiveGenerators.fixedValues(false));
		assertTrue(generator.next());
		assertFalse(generator.next());
		generator.reset();
		assertTrue(generator.next());
		assertFalse(generator.next());
	}
	
	@Test public void generateEnsuredValuesInWindow(){
		Generator<String> generator = CombinedGenerators.ensureValues(VALUES, VALUES.size(),
				new FixedValuesGenerator<String>(DISJUNCT_STRING));
		HashSet<String> rest = new HashSet<String>(VALUES);
		for(int i = 0; i < VALUES.size(); i++) assertTrue(rest.remove(generator.next()));
		assertTrue(rest.isEmpty());
		assertEquals(DISJUNCT_STRING, generator.next());
	}
	
	@Test public void generateEnsuredValuesInBiggerWindow(){
		int window = VALUES.size() * 2;
		Generator<String> generator = CombinedGenerators.ensureValues(VALUES, window,
				new FixedValuesGenerator<String>(DISJUNCT_STRING));
		HashSet<String> rest = new HashSet<String>(VALUES);
		for(int i = 0; i < window; i++) {
			String n = generator.next();
			assertTrue(rest.remove(n) || n.equals(DISJUNCT_STRING));
		}
		assertTrue(rest.isEmpty());
		assertEquals(DISJUNCT_STRING, generator.next());
	}
	
	@Test public void generateEnsuredValuesSpreadInWindow(){
		List<String> values = asList(STRING_VALUES);
		int window = values.size() * 20;
		Generator<String> generator = CombinedGenerators.ensureValues(values,
				window, new FixedValuesGenerator<String>(DISJUNCT_STRING));
		String[] c = new String[STRING_VALUES.length];
		for(int i = 0; i < c.length; i++) c[i] = generator.next();
		// Although it is not part of contract we know that the values are taken
		// in order from the ensured values iterable. If the values are nicely spread over the
		// window they should not be the first values to generate.
		// (Note: this is only were likely to work)
		assertFalse(Arrays.equals(c, STRING_VALUES));
		assert c.length == STRING_VALUES.length;
		
		generator = CombinedGenerators.ensureValues(values, c.length,
				new FixedValuesGenerator<String>(DISJUNCT_STRING));
		for(int i = 0; i < c.length; i++) c[i] = generator.next();
		assertArrayEquals("the generation order expectation is not correct", STRING_VALUES, c);
	}
	
	@Test(expected=IllegalArgumentException.class) 
	public void invalidWindowSize(){
		int invalidWindow = PrimitiveGenerators.integers(0, VALUES.size() - 1).next();
		CombinedGenerators.ensureValues(VALUES, invalidWindow, PrimitiveGenerators.<String> nulls());
	}
}

