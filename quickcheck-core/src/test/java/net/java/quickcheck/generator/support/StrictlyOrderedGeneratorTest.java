/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import net.java.quickcheck.generator.CombinedGenerators;

import org.junit.Test;

public class StrictlyOrderedGeneratorTest{

	@Test public void ordered() throws Exception {
		assertIsStrictlyOrder(CombinedGenerators.strictlyOrdered(bytes()).next());
	}
	
	@Test public void orderedSized() throws Exception {
		List<Integer> lowHigh = SizeGenerator.anyMinMax();
		int low = lowHigh.get(0);
		int high = lowHigh.get(1);
		List<Integer> actual = CombinedGenerators.strictlyOrdered(integers(), low, high).next();
		assertIsStrictlyOrder(actual);
		assertTrue(low <= actual.size());
		assertTrue(high >= actual.size());
	}
	
	@Test public void orderedComparator() throws Exception {
		List<Byte> actual = CombinedGenerators.strictlyOrdered(bytes(),
				Collections.<Byte> reverseOrder()).next();
		Collections.reverse(actual);
		assertIsStrictlyOrder(actual);
	}

	private <T extends Comparable<T>> void assertIsStrictlyOrder(List<T> l) {
		for(int i=1;i<l.size();i++){
			assertTrue(l.toString(), l.get(i - 1).compareTo(l.get(i)) < 0);
		}
	}
}
