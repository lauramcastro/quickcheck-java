/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.iterable;

import static java.util.Collections.*;
import static net.java.quickcheck.QuickCheck.*;
import static net.java.quickcheck.generator.CombinedGeneratorSamples.*;
import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGeneratorSamples.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGeneratorsIterables.*;
import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;

public class IterablesTest {

	@Test
	public void iterableDefaultNumberOfRuns() {
		int expectedNumberOfRuns = MAX_NUMBER_OF_RUNS;
		
		List<Integer> potentialValues = potentialValues(expectedNumberOfRuns);
		
		Iterable<Integer> iterable = Iterables.toIterable(ensureValues(potentialValues));
		List<Integer> actual = run(iterable);
		
		assertRun(potentialValues, expectedNumberOfRuns, actual);
	}

	@Test
	public void iterableNumberOfRuns() {
		int expectedNumberOfRuns = anyInteger(0, MAX_NUMBER_OF_RUNS);
		
		List<Integer> potentialValues = potentialValues(expectedNumberOfRuns);
		
		Iterable<Integer> iterable = Iterables.toIterable(ensureValues(potentialValues), expectedNumberOfRuns);
		List<Integer> actual = run(iterable);
		
		assertRun(potentialValues, expectedNumberOfRuns, actual);
	}
	
	@Test
	public void invalidNumberOfRuns(){
		for(int invalid : someIntegers(Integer.MIN_VALUE, -1)){
			try{
				Iterables.toIterable(strings(), invalid);
				fail();
			}catch(IllegalArgumentException e){
			}
		}
	}

	private void assertRun(List<Integer> potentialValues, int expectedNumberOfRuns, List<Integer> actual) {
		List<Integer> expected = potentialValues.subList(0, expectedNumberOfRuns);
		assertEquals(expected, actual);
	}
	
	private List<Integer> run(Iterable<Integer> iterable) {
		List<Integer> actual = new ArrayList<Integer>();
		for(Integer t : iterable) actual.add(t);
		return actual;
	}

	private List<Integer> potentialValues(int expectedNumberOfRuns) {
		int enoughValues = expectedNumberOfRuns + 1;
		List<Integer> potentialValues = lists(integers(), enoughValues, enoughValues).next();
		return potentialValues;
	}

	@Test
	public void consumedIteratorHasNext() {
		Iterator<?> iterator = consumedIterator();
		assertFalse(iterator.hasNext());
	}

	@Test(expected = NoSuchElementException.class)
	public void consumedIteratorNext() {
		Iterator<?> iterator = consumedIterator();
		iterator.next();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void removeNotSupported() {
		anyIterator().remove();
	}

	@Test(expected = IllegalArgumentException.class)
	public void generatorMayNotBeNull() {
		Iterables.toIterable(null);
	}
	
	@Test public void sizeOfWhenIterableIsEmpty() {
		assertThat(Iterables.sizeOf(emptySet()), is(0));
	}
	
	@Test public void sizeOf() {
		Set<Object> set = anyNonEmptySet(PrimitiveGenerators.objects());
		assertThat(Iterables.sizeOf(set), is(set.size()));
	}

	private Iterator<?> consumedIterator() {
		Iterator<?> iterator = anyIterator();
		while(iterator.hasNext())
			iterator.next();
		return iterator;
	}

	private Iterator<Integer> anyIterator() {
		return Iterables.toIterable(PrimitiveGenerators.integers()).iterator();
	}
}
