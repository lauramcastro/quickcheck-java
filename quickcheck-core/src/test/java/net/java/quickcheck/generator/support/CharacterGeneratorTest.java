/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.characteristic.ClassificationTestHelper.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.iterable.Iterables.*;
import static net.java.quickcheck.generator.support.CharacterGenerator.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import net.java.quickcheck.Generator;
import net.java.quickcheck.characteristic.Classification;
import net.java.quickcheck.collection.Pair;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;

public class CharacterGeneratorTest {

	@Test
	public void anyCharacters(){
		for(char c : toIterable(PrimitiveGenerators.characters())){
			assertTrue(in(c, BASIC_LATIN) || in(c, LATIN_1_SUPPLEMENT));
		}
	}
	
	@Test
	public void charactersForLowerAndUpperBounds() {
		Character lower = 'a';
		Character upper = 'f';
		
		List<Character> expected = new ArrayList<Character>();
		for(char c = lower; c <= upper; c++) expected.add(c);
		
		for(Character actual : toIterable(characters(lower, upper))) assertTrue(expected.contains(actual));
	}

	@Test(expected=IllegalArgumentException.class)
	public void generatorFixedCharactersNotEmptyArgument() {
		Character[] empty = {};
		PrimitiveGenerators.characters(empty);
	}

	@Test
	public void generatorFixedCharacters() {
		final char character = 'a';
		for(char any : toIterable(characters(character))) assertTrue(any == character);
	}

	@Test
	public void generatorMultipleFixedCharacters() {
		Character[] chars = {'a', 'x', 'q'};
		String input = "";
		for(char c : chars) input += c;
		
		Classification classification = new Classification();
		Generator<Character> generator = characters(input);
		for(Character any : toIterable(generator)) classification.classifyCall(any);
		
		assertEquals(chars.length, classification.getCategories().size());
		assertFrequencyGreater(classification, 20.0, chars);
	}
	
	private boolean in(char c, Pair<Character, Character> chars) {
		return chars.getFirst()  <= c && c <= chars.getSecond(); 
	}
	
}