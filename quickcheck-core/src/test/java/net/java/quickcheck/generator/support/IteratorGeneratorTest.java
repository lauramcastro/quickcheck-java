/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.support.IteratorGenerator.*;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsInstanceOf.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.CombinedGenerators;
import net.java.quickcheck.generator.PrimitiveGenerators;
import net.java.quickcheck.generator.distribution.Distribution;

import org.junit.Test;

public class IteratorGeneratorTest extends AbstractCollectionTestCase {

	@Test public void typeVariance(){
		Generator<Iterator<Object>> objs = CombinedGenerators.<Object> iterators(integers());
		Iterator<Object> values = objs.next();
		while(values.hasNext()) assertThat(values.next(), is(instanceOf(Integer.class)));
	}
	
	@Override
	protected Generator<Collection<Integer>> defaultGenerator() {
		return toCollectionGenerator(iterators());
	}

	private Generator<Iterator<Integer>> iterators() {
		return CombinedGenerators.iterators(PrimitiveGenerators.integers());
	}

	private Generator<Collection<Integer>> toCollectionGenerator(final Generator<Iterator<Integer>> iterators) {
		return new Generator<Collection<Integer>>() {

			@Override
			public Collection<Integer> next() {
				Iterator<Integer> iterator = iterators.next();
				Collection<Integer> collection = new ArrayList<Integer>();
				while(iterator.hasNext()) {
					collection.add(iterator.next());
				}
				return collection;
			}

		};
	}

	@Override
	protected Generator<Collection<Integer>> nonEmpty() {
		return toCollectionGenerator(CombinedGenerators.nonEmptyIterators(PrimitiveGenerators.integers()));
	}

	@Override
	protected Generator<Collection<Integer>> normalDistributionGenerator() {
		return toCollectionGenerator(CombinedGenerators.iterators(integers(),
				integers(0, MAX_SIZE, Distribution.POSITIV_NORMAL)));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void removeNotSupported() {
		Iterator<Integer> iterator = iterators().next();
		iterator.remove();
	}

}
