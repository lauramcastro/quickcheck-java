/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator;

import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.List;

import net.java.quickcheck.Generator;

import org.junit.Test;

public class GeneratorsTest {
	
	@Test
	public void integerGeneratorIsObjectGenerator(){
		Generator<Object> objects = Generators.<Object> cast(integers());
		assertTrue(objects.next() instanceof Integer);
	}
	
	@Test
	public void integerGeneratorIsIntegerGenerator(){
		Generator<Integer> objects =  Generators.cast(integers());
		assertNotNull(objects.next());
	}
	
	@Test
	public void listGeneratorIsCollectionGenerator() {
		Generator<List<Integer>> base = lists(integers());
		Generator<Collection<Integer>> objects = Generators.<Collection<Integer>> cast(base);
		assertTrue(objects.next() instanceof List<?>);
	}
}
