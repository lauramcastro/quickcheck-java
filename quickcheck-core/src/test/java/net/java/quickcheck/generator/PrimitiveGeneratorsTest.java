/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator;

import static java.lang.Boolean.*;
import static net.java.quickcheck.characteristic.ClassificationTestHelper.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static net.java.quickcheck.generator.iterable.Iterables.*;
import static org.junit.Assert.*;

import java.util.Arrays;

import net.java.quickcheck.Generator;
import net.java.quickcheck.characteristic.Classification;

import org.junit.Test;

public class PrimitiveGeneratorsTest {

	@Test
	public void testFixedValuesGenerator() {
		final Integer[] values = new Integer[] {integers().next(), integers().next(), integers().next() };
		Classification classification = new Classification();
		for(Integer i : toIterable(fixedValues(values))) {
			classification.classifyCall(i);
			assertTrue(Arrays.asList(values).contains(i));
		}
		assertFrequencyGreater(classification, 20.0, values);
	}

	enum TestEnum { A, B, C }
	
	@Test
	public void testEnums() {
		Generator<TestEnum> generator = enumValues(TestEnum.class);
		Classification classification = new Classification();
		for(TestEnum e : toIterable(generator)) classification.classifyCall(e);
		
		assertFrequencyGreater(classification, expectedFrequency(TestEnum.values().length), TestEnum.values());
	}

	private double expectedFrequency(int elements) {
		double tolerance = 0.7;
		return 100 / elements * tolerance;
	}

	@Test
	public void testEnumsExcept() {
		TestEnum excluded = enumValues(TestEnum.class).next(); 
		Generator<TestEnum> generator = enumValues(TestEnum.class, excluded);
		Classification classification = new Classification();
		for(TestEnum e : toIterable(generator)) classification.classifyCall(e);
		
		for(TestEnum e : TestEnum.values()){
			if(e == excluded) assertFrequencySmaller(classification, 0.0, e);
			else assertFrequencyGreater(classification, expectedFrequency(TestEnum.values().length - 1), e); 
		}
	}

	@Test
	public void testEnumAllExcluded() {
		try {
			PrimitiveGenerators.enumValues(TestEnum.class, TestEnum.values());
			fail();
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testBooleansGeneration() {
		Classification classification = new Classification();
		for(Boolean b : toIterable(PrimitiveGenerators.booleans())) classification.classifyCall(b);
		assertTrue(classification.getFrequency(TRUE) > 35);
		assertTrue(classification.getFrequency(FALSE) > 35);
	}
}
