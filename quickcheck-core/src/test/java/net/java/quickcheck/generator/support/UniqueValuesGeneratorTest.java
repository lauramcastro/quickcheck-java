/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;
import net.java.quickcheck.Generator;
import net.java.quickcheck.GeneratorException;
import net.java.quickcheck.MockFactory;
import net.java.quickcheck.StatefulGenerator;
import net.java.quickcheck.generator.CombinedGenerators;

import org.junit.Test;

public class UniqueValuesGeneratorTest {

	@Test public void testStopGenerationOfValuesAfterMaxTries() throws Exception {
		int tries = 100;
		Generator<Boolean> generator = MockFactory.createBooleanMock();
		expect(generator.next()).andReturn(true);
		expectLastCall().times(tries + 1);
		replay(generator);
		StatefulGenerator<Boolean> unique = uniqueValuesGenerator(tries, generator);
		assertTrue(unique.next());
		try {
			unique.next();
			fail("Generator should return only unique values");
		} catch (GeneratorException e) { }
		verify(generator);
	}

	@Test public void testGenerationOfUniqueValuesOnly() throws Exception {
		int tries = 3;
		Generator<Boolean> generator = MockFactory.createBooleanMock();
		expect(generator.next()).andReturn(true).times(tries - 1);
		expect(generator.next()).andReturn(false);
		replay(generator);

		StatefulGenerator<Boolean> unique = uniqueValuesGenerator(tries, generator);
		assertTrue(unique.next());
		assertFalse(unique.next());
		verify(generator);
	}

	@Test public void testDefaultValueForMaxTries() throws Exception {
		Generator<Boolean> generator = MockFactory.createBooleanMock();
		expect(generator.next()).andReturn(true);
		expectLastCall().times(CombinedGenerators.DEFAULT_MAX_TRIES + 1);
		replay(generator);

		StatefulGenerator<Boolean> unique = uniqueValuesGenerator(generator);
		assertEquals(true, unique.next());
		try {
			unique.next();
			fail("Generator should return only unique values");
		} catch (GeneratorException e) {
		}
		verify(generator);
	}

	@Test public void testResetOfGenerator() throws Exception {
		int tries = 1;
		Generator<Boolean> generator = MockFactory.createBooleanMock();
		expect(generator.next()).andReturn(true);
		expectLastCall().times(1 + tries + 1);
		replay(generator);

		StatefulGenerator<Boolean> unique = uniqueValuesGenerator(tries, generator);
		unique.next();
		try {
			unique.next();
			fail("Generator should return only unique values");
		} catch (GeneratorException e) {
		}
		unique.reset();
		unique.next();
		verify(generator);
	}
	
	StatefulGenerator<Boolean> uniqueValuesGenerator(int tries, Generator<Boolean> generator) {
		return CombinedGenerators.uniqueValues(generator, tries);
	}
	
	StatefulGenerator<Boolean> uniqueValuesGenerator(Generator<Boolean> generator) {
		return CombinedGenerators.uniqueValues(generator);
	}
}