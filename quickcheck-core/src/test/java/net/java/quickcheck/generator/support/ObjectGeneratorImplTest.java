/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static net.java.quickcheck.MockFactory.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.easymock.EasyMock.*;
import static org.easymock.classextension.EasyMock.*;
import static org.junit.Assert.*;
import net.java.quickcheck.Generator;
import net.java.quickcheck.ObjectGenerator;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;

public class ObjectGeneratorImplTest {

	@Test
	public void objects() {
		ObjectGenerator<Simple> generator = PrimitiveGenerators
				.objects(Simple.class);
		Integer expected = defineGeneratorValueForGetValue(generator);
		assertEquals(expected, generator.next().getValue());
	}

	private Integer defineGeneratorValueForGetValue(
			ObjectGenerator<? extends Simple> generator) {
		Integer expected = integers().next();
		Simple recorder = generator.getRecorder();
		generator.on(recorder.getValue()).returns(integerGenerator(expected));
		return expected;
	}

	@Test
	public void compoundObjects() {
		ObjectGenerator<Simple> simpleGenerator = PrimitiveGenerators
				.objects(Simple.class);
		simpleGenerator.on(simpleGenerator.getRecorder().getValue()).returns(
				integers());
		ObjectGenerator<Compound> compoundGenerator = PrimitiveGenerators
				.objects(Compound.class);
		compoundGenerator.on(compoundGenerator.getRecorder().simple()).returns(
				simpleGenerator);

		Simple simple = compoundGenerator.next().simple();
		Integer value = simple.getValue();
		assertNotNull(value);
	}

	@Test
	public void defaultObjects() {
		Generator<AllSupportedPrimitives> objects = PrimitiveGenerators
				.defaultObjects(AllSupportedPrimitives.class);
		AllSupportedPrimitives next = objects.next();
		assertNotNull(next.aByte());
		assertNotNull(next.aDouble());
		assertNotNull(next.anInt());
		assertNotNull(next.aLong());
		assertNotNull(next.aByteWrapper());
		assertNotNull(next.aDoubleWrapper());
		assertNotNull(next.anIntWrapper());
		assertNotNull(next.aLongWrapper());
		assertNotNull(next.aString());
		assertNotNull(next.aNumber());
	}

	@Test
	public void defaultCompoundObjects() {
		Generator<Compound> defaultObjects = PrimitiveGenerators
				.defaultObjects(Compound.class);
		assertNotNull(defaultObjects.next().simple().getValue());
	}

	@Test
	public void defaultObjectsMixedMode() {
		ObjectGenerator<Mixed> defaultObjects = PrimitiveGenerators
				.defaultObjects(Mixed.class);
		Integer expectedValue = defineGeneratorValueForGetValue(defaultObjects);
		Mixed next = defaultObjects.next();
		assertNotNull(next.simple().getValue());
		assertEquals(expectedValue, next.getValue());
	}

	@Test(expected = IllegalStateException.class)
	public void defaultCompoundsObjectsIgnoresClasses() {
		Generator<CompoundWithInvalidClassMember> defaultObjects = PrimitiveGenerators
				.defaultObjects(CompoundWithInvalidClassMember.class);
		defaultObjects.next();
	}

	@Test
	public void objectsReturnTypesAreCovariant() {
		ObjectGenerator<Covariant> generator = PrimitiveGenerators
				.objects(Covariant.class);
		Integer expected = integers().next();
		Covariant recorder = generator.getRecorder();
		generator.on(recorder.aNumber()).returns(integerGenerator(expected));
		assertEquals(expected, generator.next().aNumber());
	}

	@Test(expected = IllegalStateException.class)
	public void objectsDefinitionMissing() {
		ObjectGenerator<Simple> generator = PrimitiveGenerators
				.objects(Simple.class);
		generator.next();
	}

	@Test(expected = IllegalArgumentException.class)
	public void objectsClassNotSupported() {
		PrimitiveGenerators.objects(Object.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void objectsClassNull() {
		PrimitiveGenerators.objects(null);
	}

	@Test(expected = IllegalStateException.class)
	public void objectsGenerateCallWithoutGenerator() {
		Simple notARecorder = new Simple() {

			@Override
			public Integer getValue() {
				return null;
			}

			@Override
			public void setValue(Integer v) {
			}
		};
		ObjectGenerator<Simple> generator = PrimitiveGenerators
				.objects(Simple.class);
		generator.on(notARecorder.getValue());
	}

	private Generator<Integer> integerGenerator(Integer expected) {
		Generator<Integer> generator = createIntegerGenerator();
		expect(generator.next()).andReturn(expected);
		replay(generator);
		return generator;
	}

	private static interface Simple {
		Integer getValue();

		void setValue(Integer v);
	}

	private interface Compound {
		Simple simple();

		void setSimple(Simple s);
	}

	private interface Mixed extends Simple, Compound {
	}

	private interface Covariant {
		Number aNumber();
	}

	private interface CompoundWithInvalidClassMember {
		Object classesNotValid();
	}

	private static interface AllSupportedPrimitives {
		int anInt();

		long aLong();

		byte aByte();

		double aDouble();

		Integer anIntWrapper();

		Long aLongWrapper();

		Byte aByteWrapper();

		Double aDoubleWrapper();

		Number aNumber();

		String aString();
	}

}
