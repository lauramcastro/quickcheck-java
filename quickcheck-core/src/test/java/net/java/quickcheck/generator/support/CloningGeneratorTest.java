/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.generator.support;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import net.java.quickcheck.Generator;
import net.java.quickcheck.generator.PrimitiveGenerators;
import net.java.quickcheck.generator.Prototype;

import org.junit.Before;
import org.junit.Test;

public class CloningGeneratorTest {
	private Generator<Prototype> cloningGenerator;
	private Prototype prototype;

	@Before
	public void setUp() throws Exception {
		prototype = new Prototype("");
		cloningGenerator = PrimitiveGenerators.clonedValues(prototype);
	}

	@Test
	public void testCloningGeneratorPrototypeNotSameObjectAsGeneratedValue() {

		Prototype next = cloningGenerator.next();
		assertNotNull(next);
		assertTrue(prototype != next);
	}

	@Test
	public void testCloningGeneratorGeneratedValuesAreNotTheSame() {
		Prototype last = null;
		for (int i = 0; i < 10; i++) {
			Prototype next = cloningGenerator.next();
			assertTrue(next != last);
			last = next;
		}
	}

	@Test
	public void testThrowsExceptionIfNotSerializable() {

		try {
			new CloningGenerator<Object>(new Object()).next();
			fail();
		} catch (IllegalArgumentException e) {

		}
	}

}
