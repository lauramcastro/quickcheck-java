/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck;

import static java.lang.System.*;
import static net.java.quickcheck.QuickCheck.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import net.java.quickcheck.characteristic.AbstractCharacteristic;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class QuickCheckTest {

	private Characteristic<Object> characteristic;
	private Generator<Object> generator;

	@Before public void setUp() {
		characteristic = MockFactory.createObjectCharacteristic();
		generator = MockFactory.createObjectGenerator();
	}

	@After public void tearDown() {
		clearProperty(SYSTEM_PROPERTY_RUNS);
	}

	@Test public void testForAllCallCharacteristic() throws Throwable {
		expectRuns(QuickCheck.getDefaultNumberOfRuns());
		replayMocks();
		forAll(PrimitiveGenerators.fixedValues(new Object()), characteristic);
		verifyMocks();
	}

	@Test public void testForAllFailsException() throws Throwable {
		Exception exception = new Exception();
		Object last = expectExceptionThrownAfterFirstGenerator(exception);
		replayMocks();
		try {
			forAll(generator, characteristic);
		} catch (CharacteristicException e) {
			assertSame(exception, e.getCause());
			assertSame(last, e.getInstance());
			assertNotNull(e.getMessage());
		}
		verifyMocks();
	}

	private Object expectExceptionThrownAfterFirstGenerator(Exception thrown)
			throws Throwable {
		Object last = new Object();
		expect(generator.next()).andReturn(last);
		characteristic.setUp();
		characteristic.specify(last);
		expectLastCall().andThrow(thrown);
		expect(characteristic.name()).andReturn(
				PrimitiveGenerators.strings().next()).atLeastOnce();
		characteristic.tearDown();

		return last;
	}

	@Test public void testForNumberOfRuns() throws Throwable {
		int runs = 2;
		expectRuns(runs);
		replayMocks();
		QuickCheck.forAll(runs, PrimitiveGenerators.fixedValues(new Object()),
				characteristic);
		verifyMocks();
	}

	private void expectRuns(int runs) throws Throwable {
		for (int i = 0; i < runs; i++) {
			characteristic.setUp();
			characteristic.specify(anyObject());
			characteristic.tearDown();
		}
	}

	@Test public void testChoose() {
		final int lo = 10;
		final int hi = 100;
		forAll(integers(lo, hi), new AbstractCharacteristic<Integer>() {
			@Override
			public void doSpecify(Integer anyInt) {
				assertTrue(Integer.toString(anyInt), lo <= anyInt);
				assertTrue(Integer.toString(anyInt), anyInt <= hi);
			}
		});
	}

	@Test public void testForAllVerbose() throws Throwable {
		String genReturned = "returned";
		expect(generator.next()).andReturn(genReturned);
		setUpCallTearDown(genReturned);
		replayMocks();

		StringWriter writer = new StringWriter();

		RunnerImpl<Object> runner = new RunnerImpl<Object>(characteristic, 1, generator, new PrintWriter(writer));
		runner.forAll();
		String actual = writer.toString();
		String expected = "1:[returned]";
		assertEquals(expected, actual.trim());
		verifyMocks();
	}

	private void setUpCallTearDown(Object first) throws Throwable {
		characteristic.setUp();
		characteristic.specify(first);
		characteristic.tearDown();
	}

	@Test public void testForAllWithGuard() throws Throwable {
		Object first = new Object();
		Object second = new Object();
		expect(generator.next()).andReturn(first);
		expect(generator.next()).andReturn(second);
		characteristic.setUp();
		characteristic.specify(first);
		expectLastCall().andThrow(new GuardException());
		characteristic.specify(second);
		characteristic.tearDown();
		replayMocks();
		QuickCheck.forAllVerbose(1, generator, characteristic);
		verifyMocks();
	}

	@Test public void testForWithGuardAbortsAfterMaxTry() throws Throwable {
		int runs = 3;
		int maxGeneratorTries = RunnerImpl.getMaxGeneratorTries(runs);
		expect(generator.next()).andReturn(new Object()).times(
				maxGeneratorTries);
		characteristic.setUp();
		for (int i = 0; i < maxGeneratorTries; i++) {
			characteristic.specify(anyObject());
			expectLastCall().andThrow(new GuardException());
		}
		characteristic.tearDown();

		replayMocks();
		try {
			forAll(runs, generator, characteristic);
			fail();
		} catch (GeneratorException e) {

		}
		verifyMocks();
	}

	@Test
	public void testGuard() {
		guard(true);
	}

	@Test public void testGuardThrowsGuardException() {
		try {
			guard(false);
			fail();
		} catch (GuardException e) {

		}
	}



	@Test public void testGetDefaultNumberOfRuns() {
		assertEquals(MAX_NUMBER_OF_RUNS, QuickCheck.getDefaultNumberOfRuns());
	}

	@Test public void testGetDefaultNumberOfRunsOverwriteProperty() {
		Integer runs = integers(1, Integer.MAX_VALUE).next();
		System.setProperty(QuickCheck.SYSTEM_PROPERTY_RUNS, runs.toString());
		assertEquals(runs.intValue(), QuickCheck.getDefaultNumberOfRuns());
	}

	private void verifyMocks() {
		verify(characteristic, generator);
	}

	private void replayMocks() {
		replay(characteristic, generator);

	}
}