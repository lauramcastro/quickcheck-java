/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.srcgenerator;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import javax.tools.Diagnostic.Kind;

import net.java.quickcheck.collection.Triple;
import net.java.quickcheck.generator.PrimitiveGenerators;

import org.junit.Test;

public class SamplesProcessorTest {

	@Test
	public void primitive() {
		assertNotNull(UserSamples.anyPrimitive());
		assertEquals(Integer.class, UserSamples.anyPrimitive().getClass());
	}

	@Test
	public void primitiveSubtype() {
		assertNotNull(UserSamples.anyPrimitiveSubtype());
		assertEquals(Integer.class, UserSamples.anyPrimitiveSubtype()
				.getClass());
	}

	@Test
	public void complexSubtype() {
		String anyComplexSubtype = UserSamples.anyComplexSubtype();
		assertEquals(String.class, anyComplexSubtype.getClass());
	}
	
	@Test
	public void parameters() {
		Integer parameter = UserSamples.anyParameter(1, 2);
		assertEquals(Integer.class, parameter.getClass());
	}
	
	@Test
	public void bounds() {
		Kind bounded = UserSamples.anyBound(Kind.class);
		assertEquals(Kind.class, bounded.getClass());
	}
	
	@Test
	public void multipleTypeParameter(){
		Triple<Integer, Double, String> actual = UserSamples.anyMultipleTypeParameter();
		assertEquals(Integer.class, actual.getFirst().getClass());
		assertEquals(Double.class, actual.getSecond().getClass());
		assertEquals(String.class, actual.getThird().getClass());
	}
	
	@Test
	public void generics(){
		List<Integer> in = Collections.singletonList(1);
		List<Integer> is = UserSamples.anyGeneric(in);
		assertEquals(in, is);
	}
	
	@Test
	public void varArgsArray(){
		String s = PrimitiveGenerators.strings().next();
		assertEquals(s, UserSamples.anyVarArgsArray(s));
	}
	
	@Test
	public void noVarArgsArray(){
		String s = PrimitiveGenerators.strings().next();
		assertEquals(s, UserSamples.anyNoVarArgsArray(new String[]{s}, "x"));
	}

}