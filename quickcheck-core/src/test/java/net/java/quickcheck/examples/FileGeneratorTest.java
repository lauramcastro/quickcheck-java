package net.java.quickcheck.examples;

import static net.java.quickcheck.QuickCheck.*;
import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import net.java.quickcheck.Generator;
import net.java.quickcheck.characteristic.AbstractCharacteristic;

import org.junit.Test;

public class FileGeneratorTest {

	// TODO which string are supported

	@Test
	public void file() {
		forAll(new FileGenerator(), new AbstractCharacteristic<File>() {
			@Override
			protected void doSpecify(File any) {
				assertTrue(any.isAbsolute());
			}
		});
	}

	class FileGenerator implements Generator<File> {

		Generator<File> roots = fixedValues(File.listRoots());
		Generator<List<String>> paths = nonEmptyLists(letterStrings());

		@Override
		public File next() {
			File f = roots.next();
			for (String p : paths.next()) {
				f = new File(f, p);
			}
			return f;
		}
	}
}
