/*
 *  Licensed to the author under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package net.java.quickcheck.examples;

import static net.java.quickcheck.QuickCheck.*;
import static net.java.quickcheck.generator.CombinedGenerators.*;
import static net.java.quickcheck.generator.PrimitiveGenerators.*;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.List;

import net.java.quickcheck.characteristic.AbstractCharacteristic;
import net.java.quickcheck.collection.Pair;

import org.junit.Test;

public class JavaCollectionContractTest {

	@Test
	public void collectionAdd() {
		forAll(pairs(integers(), lists(integers())),
				new AbstractCharacteristic<Pair<Integer, List<Integer>>>() {

					@Override
					protected void doSpecify(Pair<Integer, List<Integer>> any)
							throws Throwable {
						Integer element = any.getFirst();
						Collection<Integer> collection = any.getSecond();

						boolean changedCollection = false;
						boolean exceptionThrown = false;
						try {
							changedCollection = collection.add(element);
						} catch (Exception e) {
							assertException(e);
							exceptionThrown = true;
						}
						assertTrue(collection.contains(element) != exceptionThrown);
						assertTrue(changedCollection == containsInstance(
								collection, element));
					}
				});
	}

	private void assertException(Exception e) {
		assertTrue(e instanceof UnsupportedOperationException
				|| e instanceof ClassCastException
				|| e instanceof IllegalArgumentException
				|| e instanceof IllegalStateException);
	}

	private boolean containsInstance(Collection<?> collection, Object element) {
		for (Object e : collection) {
			if (e == element) {
				return true;
			}
		}
		return false;
	}
}
