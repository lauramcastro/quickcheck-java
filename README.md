Quickcheck
==========

[QuickCheck](https://bitbucket.org/blob79/quickcheck) is an implementation of the [QuickCheck](http://www.haskell.org/haskellwiki/Introduction_to_QuickCheck) specification based test tool.

The goal of QuickCheck is to replace manually picked values with generated values.
A QuickCheck-based test tries to cover the laws of a domain whereas classical testing
can only test the validity for distinct values.

Basically, QuickCheck is about generators of data. The QuickCheck runner method is 
just a fancy for loop implementation. QuickCheck can help in scenarios where whole 
classes of test cases have to be tested and it is not feasible to write tests for 
all distinct test scenarios.

Generators supported for

* types ([primitive](http://blob79.bitbucket.org/quickcheck/javadoc/quickcheck_0_6/net/java/quickcheck/generator/PrimitiveGenerators.html): int, byte, long, String, char, null, boolean; collections: arrays, lists; POJOs)
* value ranges (integer, chars)
* [lists, sets and arrays](http://blob79.bitbucket.org/quickcheck/javadoc/quickcheck_0_6/net/java/quickcheck/generator/CombinedGenerators.html)
* distinct values (uniqueValues, ensureValues, fixedValues, enumValues, clonedValues, excludeValues)
* distributions
* generator strategies (composition of generator values (oneOf, frequency, list, array, nullsAnd), transformation, mutation)
* value frequencies (frequency, oneOf)
* determinism (random, deterministic )

Documentation
=============

API
---

The Quickcheck Javadoc is available [here](http://blob79.bitbucket.org/quickcheck/javadoc/quickcheck_0_6/index.html).

Example
-------

The following example test that a sorted list implementation is actual sorted. The values inserted into the list are arbitrary integer values.

```
#!java

public class SortedListTest {

  @Test public void sortedListCreation() {
     for (List<Integer> any : someLists(integers())) {
        SortedList sortedList = new SortedList(any);
        List<Integer> expected = sort(any);
        assertEquals(expected, sortedList.toList());
     }
  }
  
 private List<Integer> sort(List<Integer> any) {
     ArrayList<Integer> sorted = new ArrayList<Integer>(any);
     Collections.sort(sorted);
     return sorted;
  }
}
```

More examples can be found in the [examples](https://bitbucket.org/blob79/quickcheck/src/default/quickcheck-examples).


Releases
========

This repository is the Git version of [the original mercurial repository](https://bitbucket.org/blob79/quickcheck), as of November 22, 2015.

At that moment, latest stable release was [QuickCheck 0.6](https://bitbucket.org/blob79/quickcheck/src/quickcheck_0_6) ([zip](http://blob79.bitbucket.org/quickcheck/releases/quickcheck-0.6.zip), [javadoc](http://blob79.bitbucket.org/quickcheck/javadoc/quickcheck_0_6)) March 19, 2011 [Release notes](http://theyougen.blogspot.com/2011/05/quickcheck-06-release.html)

Maven
=====

That the following dependecy to your pom file:

```
#!xml
<dependency>
	<groupId>net.java.quickcheck</groupId>
	<artifactId>quickcheck</artifactId>
	<version>0.6</version>
	<scope>test</scope>
</dependency>
```


Help
====

Getting started
---------------

1. install Oracle JDK 7 or 8
1. git clone https://bitbucket.org/lauramcastro/quickcheck-java
1. mvn test

